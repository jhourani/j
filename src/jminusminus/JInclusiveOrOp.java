package jminusminus;

import java.util.ArrayList;

/**
 * The AST node for an inclusive OR (|) expression.
 * @author Jordan
 *
 */
class JInclusiveOrOp extends JExpression {
	

	protected JExpression lhs;
	
	protected ArrayList<JExpression> rhs_list;
	
	 /**
     * Construct an AST for an inclusive OR expression given its line number,
     * and the lhs and rhs operands.
     * 
     * @param line
     *            line in which the inclusive-OR expression occurs in the
     *            source file.
     * @param lhs
     *            the lhs operand.
     * @param rhs
     *            the rhs operand.
     */
	public JInclusiveOrOp(int line , JExpression lhs , 
			ArrayList<JExpression> rhs_list) {
		super(line);
		this.lhs = lhs;
		this.rhs_list = rhs_list;
	}
	
	public JExpression analyze(Context context) {
		//TODO
		return null;
	}
	
	public void codegen(CLEmitter output) {
		//TODO
	}
	
    /**
     * @inheritDoc
     */

    public void writeToStdOut(PrettyPrinter p) {
        p.printf("<JInclusiveOrOp line=\"%d\" type=\"%s\"/>\n", line(),
                ((type == null) ? "" : type.toString()));
        p.indentRight();
        p.println("<lhs>");
        p.indentRight();
        lhs.writeToStdOut(p);
        p.indentLeft();
        p.println("</lhs>");
        if (rhs_list != null) {
            for (JExpression argument : rhs_list) {
                p.println("<rhs>");
                p.indentRight();
                argument.writeToStdOut(p);
                p.indentLeft();
                p.println("</rhs>");
                p.indentLeft();
            }
        }
        p.indentLeft();
        p.println("</JInclusiveOrOp>");
    }
}

// Copyright 2013 Bill Campbell, Swami Iyer and Bahar Akbal-Delibas

package jminusminus;

import java.util.ArrayList;
import static jminusminus.CLConstants.*;

/**
 * The AST node for a method declaration.
 */

class JThrowMethodDeclaration extends JAST implements JMember {

    /** Method modifiers. */
    protected ArrayList<String> mods;

    /** Method name. */
    protected String name;

    /** Return type. */
    private Type returnType;

    /** The formal parameters. */
    protected ArrayList<JFormalParameter> params;
    
    /** The thrown qualified identifiers. */
    protected ArrayList<Type> qualifiedIdentifiers;

    /** Method body. */
    protected JBlock body;

    /** Built in analyze(). */
    protected MethodContext context;

    /** Computed by preAnalyze(). */
    protected String descriptor;

    /** Is method abstract. */
    protected boolean isAbstract;

    /** Is method static. */
    protected boolean isStatic;

    /** Is method private. */
    protected boolean isPrivate;

    /**
     * Construct an AST node for a method declaration given the line number,
     * method name, return type, formal parameters, and the method body.
     * 
     * @param line
     *            line in which the method declaration occurs in the source
     *            file.
     * @param mods
     *            modifiers.
     * @param name
     *            method name.
     * @param returnType
     *            return type.
     * @param params
     *            the formal parameters.
     * @param qualifiedIdentifiers
     * 			  the thrown qualified identifiers
     * @param body
     *            method body.
     */

    public JThrowMethodDeclaration(int line, ArrayList<String> mods, String name,
            Type returnType, ArrayList<JFormalParameter> params, 
            ArrayList<Type> qualifiedIdentifiers, JBlock body)

    {
        super(line);
        this.mods = mods;
        this.name = name;
        this.returnType = returnType;
        this.params = params;
        this.qualifiedIdentifiers = qualifiedIdentifiers;
        this.body = body;
        this.isAbstract = mods.contains("abstract");
        this.isStatic = mods.contains("static");
        this.isPrivate = mods.contains("private");
    }

    /**
     * TODO
     */

    public void preAnalyze(Context context, CLEmitter partial) {
        ;
    }

    /**
     * TODO
     */

    public JAST analyze(Context context) {
        return null;
    }

    /**
     * TODO
     */
    public void partialCodegen(Context context, CLEmitter partial) {
        ;
    }

    /**
     * TODO
     */

    public void codegen(CLEmitter output) {
        ;
    }

    /**
     * @inheritDoc
     */

    public void writeToStdOut(PrettyPrinter p) {
        p.printf("<JThrowMethodDeclaration line=\"%d\" name=\"%s\" "
                + "returnType=\"%s\">\n", line(), name, returnType.toString());
        p.indentRight();
        if (context != null) {
            context.writeToStdOut(p);
        }
        if (mods != null) {
            p.println("<Modifiers>");
            p.indentRight();
            for (String mod : mods) {
                p.printf("<Modifier name=\"%s\"/>\n", mod);
            }
            p.indentLeft();
            p.println("</Modifiers>");
        }
        if (params != null) {
            p.println("<FormalParameters>");
            for (JFormalParameter param : params) {
                p.indentRight();
                param.writeToStdOut(p);
                p.indentLeft();
            }
            p.println("</FormalParameters>");
        }
        if (body != null) {
            p.println("<Body>");
            p.indentRight();
            body.writeToStdOut(p);
            p.indentLeft();
            p.println("</Body>");
        }
        if (qualifiedIdentifiers != null) {
        	p.println("<ThrownQualifiedIdentifiers>");
            for (Type t : qualifiedIdentifiers) {
                p.indentRight();
                p.println(t.toString());
                p.indentLeft();
            }
            p.println("</ThrownQualifiedIdentifiers>");
        }
        p.indentLeft();
        p.println("</JThrowMethodDeclaration>");
    }

}

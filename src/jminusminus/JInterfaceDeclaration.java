// Copyright 2011 Bill Campbell, Swami Iyer and Bahar Akbal-Delibas

package jminusminus;

import java.util.ArrayList;
import static jminusminus.CLConstants.*;

/**
 * An interface declaration has a list of modifiers, a name, a super
 * classes and a class block; it distinguishes between instance
 * fields and static (class) fields for initialization, and it
 * defines a type. It also introduces its own (class) context.
 */

class JInterfaceDeclaration
    extends JAST implements JTypeDecl, JMember {

    /** Interface modifiers. */
    private ArrayList<String> mods;

    /** Interface name. */
    private String name; private Type superType;

    /** Interface block. */
    private ArrayList<JMember> interfaceBlock;

    /** Super classes type. */
    private ArrayList<TypeName> extend;

    /** This interface type. */
    private Type thisType;

    /** Context for this interface. */
    private ClassContext context;

    /** Whether this class has an explicit constructor. */
    private boolean hasExplicitConstructor;

    /** Instance fields of this class. */
    private ArrayList<JFieldDeclaration> instanceFieldInitializations;

    /** Static (class) fields of this class. */
    private ArrayList<JFieldDeclaration> staticFieldInitializations;

    /**
     * Construct an AST node for an interface declaration given the
     * line number, list of class modifiers, name of the interface,
     * its super class type, and the interface block.
     * 
     * @param line
     *                line in which the interface declaration occurs
     *                in the source file.
     * @param mods
     *                interface modifiers.
     * @param name
     *                interface name.
     * @param extend
     *                list super class type.
     * @param interfaceBlock
     *                interface block.
     */

    public JInterfaceDeclaration(int line, ArrayList<String> mods,
        String name, ArrayList<TypeName> extend, ArrayList<JMember> interfaceBlock) {
        super(line);
        this.mods = mods;
        this.name = name;
        this.extend = extend;
        this.interfaceBlock = interfaceBlock;
        hasExplicitConstructor = false;
        instanceFieldInitializations = new ArrayList<JFieldDeclaration>();
        staticFieldInitializations = new ArrayList<JFieldDeclaration>();
    }

    /**
     * Return the interface name.
     * 
     * @return the interface name.
     */

    public String name() {
        return name;
    }

    /**
     * Return the class' super class type.
     * 
     * @return the super class type.
     */

    public Type superType() {
        return superType;
    }
    
    /**
     * Return the type that this interface declaration defines.
     * 
     * @return the defined type.
     */

    public Type thisType() {
        return thisType;
    }

    /**
     * The initializations for instance fields (now expressed as
     * assignment statments).
     * 
     * @return the field declarations having initializations.
     */

    public ArrayList<JFieldDeclaration> instanceFieldInitializations() {
        return instanceFieldInitializations;
    }

    /**
     * Declare this interface in the parent (compilation unit)
     * context.
     * 
     * @param context
     *                the parent (compilation unit) context.
     */

    public void declareThisType(Context context) {
    }

    /**
     * Pre-analyze the members of this declaration in the parent
     * context. Pre-analysis extends to the member headers
     * (including method headers) but not into the bodies.
     * 
     * @param context
     *                the parent (compilation unit) context.
     */

    public void preAnalyze(Context context) {
    }

    /**
     * 
     * @param context
     * @param partial
     */
    public void preAnalyze(Context context, CLEmitter partial) {
    }
    
    /**
     * Perform semantic analysis on the interface and all of its
     * members within the given context. Analysis includes field
     * initializations and the method bodies.
     * 
     * @param context
     *                the parent (compilation unit) context.
     *                Ignored here.
     * @return the analyzed (and possibly rewritten) AST subtree.
     */

    public JAST analyze(Context context) {
        return this;
    }

    /**
     * Generate code for the interface declaration.
     * 
     * @param output
     *                the code emitter (basically an abstraction
     *                for producing the .class file).
     */

    public void codegen(CLEmitter output) {
    }

    /**
     * @inheritDoc
     */

    public void writeToStdOut(PrettyPrinter p) {
        p.printf("<JInterfaceDeclaration line=\"%d\" name=\"%s\">\n",
        		line(), name);
        p.indentRight();
        if (context != null) {
            context.writeToStdOut(p);
        }
        if (mods != null) {
            p.println("<Modifiers>");
            p.indentRight();
            for (String mod : mods) {
                p.printf("<Modifier name=\"%s\"/>\n", mod);
            }
            p.indentLeft();
            p.println("</Modifiers>");
        }
        if (extend != null) {
            p.println("<Extends>");
            p.indentRight();
            for (TypeName extended : extend) {
                p.printf("<Extends name=\"%s\"/>\n", extended
                    .toString());
            }
            p.indentLeft();
            p.println("</Extends>");
        }
        if (interfaceBlock != null) {
            p.println("<InterfaceBlock>");
            for (JMember member : interfaceBlock) {
                ((JAST) member).writeToStdOut(p);
            }
            p.println("</InterfaceBlock>");
        }
        p.indentLeft();
        p.println("</JInterfaceDeclaration>");
    }

    /**
     * Generate code for an implicit empty constructor.
     * (Necessary only if there is not already an explicit one.)
     * 
     * @param partial
     *                the code emitter (basically an abstraction
     *                for producing a Java class).
     */

    private void codegenPartialImplicitConstructor(){
    }

    /**
     * Generate code for an implicit empty constructor.
     * (Necessary only if there is not already an explicit one.
     * 
     * @param output
     *                the code emitter (basically an abstraction
     *                for producing the .class file).
     */

    private void codegenImplicitConstructor(CLEmitter output) {
    }

    /**
     * Generate code for interface initialization, in j-- this means
     * static field initializations.
     * 
     * @param output
     *                the code emitter (basically an abstraction
     *                for producing the .class file).
     */

    private void codegenClassInit(CLEmitter output) {
    }
}
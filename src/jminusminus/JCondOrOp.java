package jminusminus;

import java.util.ArrayList;

/**
 * The AST node for a condition-OR ( || ) expression. It keeps track of its
 * type, the Constructor representing the expression, its arguments and their
 * types.
 */

class JCondOrOp extends JExpression {

    /** The arguments to the constructor. */
    private ArrayList<JExpression> rhs_list;

    JExpression lhs;

    /**
     * Construct an AST node for a conditional-OR ( || ) expression.
     * 
     * @param line
     *            the line in which the OR expression occurs in the source
     *            file.
     * @param type
     *            the type being constructed.
     * @param arguments
     *            arguments to the constructor.
     */

    public JCondOrOp(int line, JExpression lhs,
    		ArrayList<JExpression> arguments) {
        super(line);
        this.lhs = lhs;
        this.rhs_list = arguments;
    }

    /**
     * TODO
     */
    public JExpression analyze(Context context) {
        return null;
    }
    
    /**
     * TODO
     */
    public void codegen(CLEmitter output) {
        
    }

    /**
     * @inheritDoc
     */

    public void writeToStdOut(PrettyPrinter p) {
        p.printf("<JCondOrOp line=\"%d\" type=\"%s\"/>\n", line(),
                ((type == null) ? "" : type.toString()));
        p.indentRight();
        if (rhs_list != null) {
            p.println("<Arguments>");
            for (JExpression argument : rhs_list) {
                p.indentRight();
                p.println("<Argument>");
                p.indentRight();
                argument.writeToStdOut(p);
                p.indentLeft();
                p.println("</Argument>");
                p.indentLeft();
            }
            p.println("</Arguments>");
        }
        p.indentLeft();
        p.println("</JCondOrOp>");
    }

}

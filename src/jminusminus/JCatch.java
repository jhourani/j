// Copyright 2011 Bill Campbell, Swami Iyer and Bahar Akbal-Delibas

package jminusminus;

import java.util.ArrayList;

/**
 * The AST node for a Catch block.
 */

class JCatch
    extends JStatement implements JMember{

    /** List of statements forming the catch body. */
    private JBlock block;

    /** The formal parameter. */
    private JFormalParameter param;
    
    /**
     * The new context (built in analyze()) represented by this
     * block.
     */
    private LocalContext context;

    /**
     * Construct an AST node for a catch block given its line number,
     * and the list of statements forming the block body.
     * 
     * @param line
     *                line in which the block occurs in the
     *                source file.
     * @param param
     *                list of formal parameters.
     * @param block
     *                the block itself.
     */

    public JCatch(int line, JFormalParameter param, JBlock block) {
        super(line);
        this.block = block;
        this.param = param;
    }

    /**
     * Return the list of statements comprising the block.
     * 
     * @return list of statements.
     */

    public JBlock block() {
        return block;
    }

    /**
     * Analyzing a block consists of creating a new nested
     * context for that block and analyzing each of its
     * statements within that context.
     * 
     * @param context
     *                context in which names are resolved.
     * @return the analyzed (and possibly rewritten) AST subtree.
     */

    public JCatch analyze(Context context) {
        return this;
    }

    /**
     * Generating code for a block consists of generating code
     * for each of its statements.
     * 
     * @param output
     *                the code emitter (basically an abstraction
     *                for producing the .class file).
     */

    public void codegen(CLEmitter output) {
    }
    /**
     * 
     * @param context
     * @param partial
     */
    public void preAnalyze(Context context, CLEmitter partial) {
    }

    /**
     * @inheritDoc
     */

    public void writeToStdOut(PrettyPrinter p) {
        p.printf("<JCatch line=\"%d\">\n", line());
        p.indentRight();
        if (context != null) {
            p.indentRight();
            context.writeToStdOut(p);
            p.indentLeft();
        }
        if (param != null) {
            p.println("<FormalParameter>");
                p.indentRight();
                param.writeToStdOut(p);
                p.indentLeft();
            p.println("</FormalParameter>");
        }
        if (block != null) {
            p.println("<CatchBlock>");
            p.indentRight();
            block.writeToStdOut(p);
            p.indentLeft();
            p.println("</CatchBlock>");
        }
        p.indentLeft();
        p.printf("</JCatch>\n");
    }
}
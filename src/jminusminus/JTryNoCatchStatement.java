/**
 * author : Jordan Hourani
 * added as part of homework #3 - Parser
 */

package jminusminus;

import static jminusminus.CLConstants.*;

/**
 * The AST node for a try statement without a catch statement.
 */

class JTryNoCatchStatement extends JStatement {

	/** a block statement */
	JBlock block;

    /**
     * Construct an AST node for a try-statement given its line number, and
     * a block.
     * 
     * @param line
     *            line in which the try-statement appears in the source file.
     */

    public JTryNoCatchStatement(int line, JBlock block) {
        super(line);
        this.block = block;
    }

    /**
     * TODO
     */

    public JStatement analyze(Context context) {
        return null;
    }

    /**
     * 
     */

    public void codegen(CLEmitter output) {
        //TODO
    }

    /**
     * @inheritDoc
     */

    public void writeToStdOut(PrettyPrinter p) {
            p.printf("<JTryNoCatchStatement line=\"%d\"/>\n", line());
            
    }

}

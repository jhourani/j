// Copyright 2011 Bill Campbell, Swami Iyer and Bahar Akbal-Delibas

package jminusminus;
import java.util.ArrayList;
import static jminusminus.CLConstants.*;

/**
 * The AST node for a Enhanced For-statement.
 */

class JEnhancedForStatement
    extends JStatement {

	/** Type. */
    private Type type;
    
    /** Identifier name. */
    private String name;
    
	/** Initialization expression. */
    private JStatement expre;

    /** The body. */
    private JStatement body;

    /**
     * Construct an AST node for a enhanced For-statement given its line
     * number, the test expression, and the body.
     * 
     * @param line
     *                line in which the while-statement occurs in
     *                the source file.
     * @param type
     *                variable type.
     * @param name
     *                name of variable.
     * @param expre
     *                Expression for loop.                              
     * @param body
     *                the body.
     */

    public JEnhancedForStatement(int line, Type type, String name,
        JStatement expre, JStatement body) {
        super(line);
        this.type = type;
        this.name = name;
        this.expre = expre;
        this.body = body;
    }
    
    /**
     * Return the variable name.
     * 
     * @return the variable name.
     */

    public String name() {
        return name;
    }

    /**
     * Analysis involves analyzing the test, checking its type
     * and analyzing the body statement.
     * 
     * @param context
     *                context in which names are resolved.
     * @return the analyzed (and possibly rewritten) AST subtree.
     */

    public JEnhancedForStatement analyze(Context context) {
        return this;
    }

    /**
     * Generate code for the enhanced For loop. 
     * 
     * @param output
     *                the code emitter (basically an abstraction
     *                for producing the .class file).
     */

    public void codegen(CLEmitter output) {
    }

    /**
     * @inheritDoc
     */

    public void writeToStdOut(PrettyPrinter p) {
        p.printf("<JEnhancedForStatement line=\"%d\">\n", line());
        p.indentRight();
        
        p.printf("<JForInitializer name=\"%s\" "
                + "type=\"%s\">\n", name, type.toString());
        p.indentRight();
            
        p.printf("<Expression>\n");
        p.indentRight();
        expre.writeToStdOut(p);
        p.indentLeft();
        p.printf("</Expression>\n");
        
        p.printf("<Body>\n");
        p.indentRight();
        body.writeToStdOut(p);
        p.indentLeft();
        p.printf("</Body>\n");
        
        p.indentLeft();
        p.printf("</JForInitializer>\n");
        p.indentLeft();
        p.printf("</JEnhancedForStatement>\n");
    }
}
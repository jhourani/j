// Copyright 2013 Bill Campbell, Swami Iyer and Bahar Akbal-Delibas

package jminusminus;

/**
 * The AST node for a float literal.
 */

class JLiteralFloat extends JExpression {

    /** String representation of the char. */
    private String text;

    /**
     * Construct an AST node for a float literal given its line number and text
     * representation.
     * 
     * @param line
     *            line in which the literal occurs in the source file.
     * @param text
     *            string representation of the literal.
     */

    public JLiteralFloat(int line, String text) {
        super(line);
        this.text = text;
    }

    //TODO fill in analyze()
    public JExpression analyze(Context context) {
        return null;
    }
    
    public void codegen(CLEmitter output) {
    	//TODO 
    }

    /**
     * @inheritDoc
     */

    public void writeToStdOut(PrettyPrinter p) {
        p.printf("<JLiteralFloat line=\"%d\" type=\"%s\" " + "value=\"%s\"/>\n",
                line(), ((type == null) ? "" : type.toString()), Util
                        .escapeSpecialXMLChars(text));
    }

}

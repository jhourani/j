// Copyright 2011 Bill Campbell, Swami Iyer and Bahar Akbal-Delibas

package jminusminus;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Hashtable;
import static jminusminus.TokenKind.*;

/**
 * A lexical analyzer for j--, that has no backtracking
 * mechanism.
 * 
 * When you add a new token to the scanner, you must also add an
 * entry in the TokenKind enum in TokenInfo.java specifying the
 * kind and image of the new token.
 */

class Scanner {

    /** End of file character. */
    public final static char EOFCH = CharReader.EOFCH;

    /** Keywords in j--. */
    private Hashtable<String, TokenKind> reserved;

    /** Source characters. */
    private CharReader input;

    /** Next unscanned character. */
    private char ch;

    /** Whether a scanner error has been found. */
    private boolean isInError;

    /** Source file name. */
    private String fileName;

    /** Line number of current token. */
    private int line;
    
    private boolean errorfound = false;

    /**
     * Construct a Scanner object.
     * 
     * @param fileName
     *                the name of the file containing the source.
     * @exception FileNotFoundException
     *                    when the named file cannot be found.
     */

    public Scanner(String fileName) throws FileNotFoundException {
        this.input = new CharReader(fileName);
        this.fileName = fileName;
        isInError = false;

        // Keywords in j--
        reserved = new Hashtable<String, TokenKind>();
        reserved.put(ABSTRACT.image(), ABSTRACT);
        reserved.put(BOOLEAN.image(), BOOLEAN);
        reserved.put(CHAR.image(), CHAR);
        reserved.put(CLASS.image(), CLASS);
        reserved.put(ELSE.image(), ELSE);
        reserved.put(EXTENDS.image(), EXTENDS);
        reserved.put(FALSE.image(), FALSE);
        reserved.put(IF.image(), IF);
        reserved.put(IMPORT.image(), IMPORT);
        reserved.put(INSTANCEOF.image(), INSTANCEOF);
        reserved.put(INT.image(), INT);
        reserved.put(NEW.image(), NEW);
        reserved.put(NULL.image(), NULL);
        reserved.put(PACKAGE.image(), PACKAGE);
        reserved.put(PRIVATE.image(), PRIVATE);
        reserved.put(PROTECTED.image(), PROTECTED);
        reserved.put(PUBLIC.image(), PUBLIC);
        reserved.put(RETURN.image(), RETURN);
        reserved.put(STATIC.image(), STATIC);
        reserved.put(SUPER.image(), SUPER);
        reserved.put(THIS.image(), THIS);
        reserved.put(TRUE.image(), TRUE);
        reserved.put(VOID.image(), VOID);
        reserved.put(WHILE.image(), WHILE);
        //added for Assignment #2
        reserved.put(GOTO.image(), GOTO);
        reserved.put(CONTINUE.image(), CONTINUE);
        reserved.put(FOR.image(), FOR);
        reserved.put(SWITCH.image(), SWITCH);
        reserved.put(DEFAULT.image(), DEFAULT);
        reserved.put(SYNCHRONIZED.image(), SYNCHRONIZED);
        reserved.put(DO.image(), DO);
        reserved.put(BREAK.image(), BREAK);
        reserved.put(DOUBLE.image(), DOUBLE);
        reserved.put(IMPLEMENTS.image(), IMPLEMENTS);
        reserved.put(THROW.image(), THROW);
        reserved.put(BYTE.image(), BYTE);
        reserved.put(TRANSIENT.image(), TRANSIENT);
        reserved.put(CASE.image(), CASE);
        reserved.put(THROWS.image(), THROWS);
        reserved.put(CATCH.image(), CATCH);
        reserved.put(INTERFACE.image(), INTERFACE);
        reserved.put(SHORT.image(), SHORT);
        reserved.put(TRY.image(), TRY);
        reserved.put(FINAL.image(), FINAL);
        reserved.put(FINALLY.image(), FINALLY);
        reserved.put(LONG.image(), LONG);
        reserved.put(STRICTFP.image(), STRICTFP);
        reserved.put(NATIVE.image(), NATIVE);
        reserved.put(CONST.image(), CONST);
        reserved.put(FLOAT.image(), FLOAT);
        reserved.put(VOLATILE.image(), VOLATILE);
        reserved.put(ASSERT.image(), ASSERT);
        reserved.put(ENUM.image(), ENUM);

        // Prime the pump.
        nextCh();
    }

    /**
     * Scan the next token from input.
     * 
     * @return the the next scanned token.
     */

    public TokenInfo getNextToken() {
        StringBuffer buffer;
        boolean moreWhiteSpace = true;
        boolean cursor = true;
        while (moreWhiteSpace) 
        {
        	while (isWhitespace(ch)) 
        		nextCh();
               
        	if (ch == '/') 
        	{
        		nextCh();
        		if (ch == '/') 
        		{
        			// CharReader maps all new lines to '\n'
        			while (ch != '\n' && ch != EOFCH) 
        				nextCh();
        		}
        		else if (ch == '*') 
        		{
        			cursor = true;
        			while (cursor) 
        			{
        				nextCh();
        				if (ch == '*')
        				{
        					nextCh();
        					if (ch == '/')
        					{
        						nextCh();
        						cursor = false;
        					}
        				}
        				else if(ch == EOFCH)
        				{
        					cursor = false;
        					reportScannerError
						    ("Unclosed Comments!");
        				}
        			}
        		}
        		else if (ch == '=') 
        		{
        			nextCh();
        			return new TokenInfo(DIV_ASSIGN, line);
        		} 
        		else 
        			return new TokenInfo(DIV, line);
                 
        	}  
        	else 
        		moreWhiteSpace = false;
        }
              
        line = input.line();
        switch (ch) {
        case '(':
            nextCh();
            return new TokenInfo(LPAREN, line);
        case ')':
            nextCh();
            return new TokenInfo(RPAREN, line);
        case '{':
            nextCh();
            return new TokenInfo(LCURLY, line);
        case '}':
            nextCh();
            return new TokenInfo(RCURLY, line);
        case '[':
            nextCh();
            return new TokenInfo(LBRACK, line);
        case ']':
            nextCh();
            return new TokenInfo(RBRACK, line);
        case ';':
            nextCh();
            return new TokenInfo(SEMI, line);
        case ':':
            nextCh();
            return new TokenInfo(COLON, line);
        case ',':
            nextCh();
            return new TokenInfo(COMMA, line);
        case '=':
            nextCh();
            if (ch == '=') {
                nextCh();
                return new TokenInfo(EQUAL, line);
            } else {
                return new TokenInfo(ASSIGN, line);
            }
        case '!':
            nextCh();
            if (ch == '=')
            {
                nextCh();
                return new TokenInfo(NOT_EQUAL, line);
            }
            else
            	return new TokenInfo(LNOT, line);
        case '*':
            nextCh();
            if (ch == '=')
            {
                nextCh();
                return new TokenInfo(STAR_ASSIGN, line);
            } 
            else 
                return new TokenInfo(STAR, line);
        case '%':
            nextCh();
            if (ch == '=') 
            {
                nextCh();
                return new TokenInfo(MOD_ASSIGN, line);
            }
            else 
                return new TokenInfo(MOD, line);
            
        case '|':
            nextCh();
            if (ch == '=') 
            {
            	nextCh();
            	return new TokenInfo(BOR_ASSIGN, line);
            } 
            else if (ch == '|') 
            {
            	nextCh();
            	return new TokenInfo(LOR, line);
            } 
            else 
            	return new TokenInfo(OR, line);
        case '^':
            nextCh();
            if (ch == '=') 
            {
                nextCh();
                return new TokenInfo(BXOR_ASSIGN, line);
            } 
            else 
                return new TokenInfo(XOR, line);
        case '~':
            nextCh();
            return new TokenInfo(COMPL, line);
        case '?':
            nextCh();
            return new TokenInfo(QUESTION, line);
        case '+':
            nextCh();
            if (ch == '=') {
                nextCh();
                return new TokenInfo(PLUS_ASSIGN, line);
            } else if (ch == '+') {
                nextCh();
                return new TokenInfo(INC, line);
            } else {
                return new TokenInfo(PLUS, line);
            }
        case '-':
            nextCh();
            if (ch == '-') 
            {
                nextCh();
                return new TokenInfo(DEC, line);
            } 
            else if (ch == '=')
            {
                nextCh();
                return new TokenInfo(MINUS_ASSIGN, line);
            }
            else
                return new TokenInfo(MINUS, line);
        case '&':
            nextCh();
            if (ch == '=') 
            {
            	nextCh();
            	return new TokenInfo(BAND_ASSIGN, line);
            } 
            else if (ch == '&') 
            {
            	nextCh();
            	return new TokenInfo(LAND, line);
            } 
            else
            	return new TokenInfo(AND, line);            
        case '>':
            nextCh();
            if (ch == '=') 
            {
            	nextCh();
            	return new TokenInfo(GE, line);
            } 
            else if (ch == '>')
            {
            	nextCh();
            	if (ch == '=') 
            	{
            		nextCh();
            		return new TokenInfo(SR_ASSIGN, line);
            	} 
            	else if (ch == '>') 
            	{
            		nextCh();
            		if (ch == '=') 
            		{
            			nextCh();
            			return new TokenInfo(BSR_ASSIGN, line);
            		} 
            		else 
            			return new TokenInfo(BSR, line);
            	} 
            	else 
            		return new TokenInfo(SR, line);
            }
            else 
             return new TokenInfo(GT, line);
        case '<':
            nextCh();
            if (ch == '=') 
            {
            	nextCh();
            	return new TokenInfo(LE, line);
            } 
            else if (ch == '<') 
            {
            	nextCh();
                if (ch == '=') 
                {
                	nextCh();
                	return new TokenInfo(SL_ASSIGN, line);
                }
                else 
                    return new TokenInfo(SL, line);
            } 
            else 
            	return new TokenInfo(LT, line);
            
        case '\'':   //case for char literal'(ESC|OCTAL_ESC_~('|\|LF|CR))'
            buffer = new StringBuffer();
            buffer.append('\'');
            boolean isUni = false;
            boolean isOct = false;
            boolean error = false;
            nextCh();
            if (ch == '\\')  //This begins the Escape, either Unicode or Octal
            {
            	nextCh();
                if(ch == 'u')	//This is start of unicode can follow many u's
                {
                	isUni = true;
                	buffer.append('\\');
                	buffer.append(ch);
                	nextCh();
                	while(ch == 'u') // Append all u's before reading  
	                {		// Hex-digit		
                		buffer.append(ch);
                		nextCh();
                	}
                	for(int i = 0 ; i < 4; i++) // must follow 4 Hex-digit
                	{
                		if(isDigit(ch) || (ch >= 'a' && ch <= 'f') || 
            					(ch >= 'A' && ch <= 'F'))
                		{
                			buffer.append(ch);
                			nextCh();
                		}
                		else // Condition where unicode does not follow
                		{    // 4 Hex-digit Eat up the whole Unicode
				     // and prints error
                			while(ch != '\'' && ch != ';' 
					      && ch != '\n' && ch != EOFCH)
                			{
                				buffer.append(ch);
                				nextCh();
                			}
                			buffer.append(ch);
                			reportScannerError
					    ("Badly formed Unicode escape. %s",
                					buffer.toString());
                			nextCh();
                			return getNextToken(); // Get next token
                		}                              // after error
                	}
                }	
                else if(ch >= '0' && ch <= '7') // This is a start of Octal 
                {			        // Escape, can have one, two or
                	isOct = true;		// three octal digits and number
                	boolean hundreds = false;  //must not exceed 377	
                	if(ch >= '0' && ch <= '3')
                		hundreds = true; // Here we flag if the first 
                	buffer.append('\\');	 // digit is 0,1,2 or 3 
                	buffer.append(ch);   
                	nextCh();
                	for(int i = 0 ; i < 2 && !error; i++)
                	{
                		if(ch >= '0' && ch <= '7')
                		{
                			buffer.append(ch);
                    		nextCh();
                		}
                		else if(ch == '\'') //case where we don't see 3 
				                    //digit and break 
                			break;	    // out of loop
                		else
                			error = true;
                		if(i == 1 && !hundreds) 
                		{		        
                			error = true;
                			break;
                		}
                	}
                	if(error) //In case of bad Octal Escape print error
			{         
                		while(ch != '\'' && ch != ';' && ch != '\n' 
				      && ch != EOFCH)
            			{
            				buffer.append(ch);
            				nextCh();
            			}
            			buffer.append(ch);
            			reportScannerError
				    ("Badly formed Octal escape. %s",
            					buffer.toString());
            			nextCh();
            			return getNextToken();
                	}
                	
                }
                else
                {
                	String temp = escape();
                	if(temp.length() != 0)
                		buffer.append(temp);
                	else
                	{
                		while(ch != '\'' && ch != ';' && ch != '\n' 
				      && ch != EOFCH)
                		{
                			buffer.append(ch);
                			nextCh();
                		}
                		nextCh();
                		return getNextToken();
                	}
                }
            }
            else if (ch == '\'') //This is the case for Emtpy Char ''
	    {                    //which results in error
                buffer.append(ch);
                nextCh();
                reportScannerError("%s -  Error, empty character literal", 
				   buffer.toString());
                return getNextToken(); 
                
            }
            else if(ch == EOFCH) // This is the case where ' follows EOF
            {
                reportScannerError("Unexpected end of file reached");
                return getNextToken();
            }
            else 
            {
                buffer.append(ch);
                nextCh();
            }
            if (ch == '\'') {	//This is end of Char
                buffer.append('\'');
                nextCh();
                //This checks if the Unicode is not resulting in LF|CR|'|\  
                if(isUni && 	    //which are invalid
                	 ((buffer.substring(buffer.length()-5,
                	   buffer.length()-1).equalsIgnoreCase("000a")) ||
                	  (buffer.substring(buffer.length()-5,
                	   buffer.length()-1).equalsIgnoreCase("000d")) ||
                	  (buffer.substring(buffer.length()-5,
                           buffer.length()-1).equalsIgnoreCase("005c")) ||
                          (buffer.substring(buffer.length()-5,
                           buffer.length()-1).equalsIgnoreCase("0027"))))
                {
                	reportScannerError("Not Valid Unicode Char literal. %s",
					   buffer.toString());
                	return getNextToken();
                }
                else
                	return new TokenInfo(CHAR_LITERAL, 
					     buffer.toString(), line);
            } else {
                // Expected a ' ; report error and try to
                // recover.
                reportScannerError(ch
                    + " found by scanner where closing ' was expected.");
                while (ch != '\'' && ch != ';' && ch != '\n') {
                    nextCh();
                }
                nextCh();
                return getNextToken();
                //return new TokenInfo(CHAR_LITERAL, buffer.toString(), line);
            }
        case '"':      //String can have Unicode Escape or Octal or Both
            buffer = new StringBuffer();	//"{ESC|OCTAL_ESC|~("|\|LF|CR)}'
            boolean isUni2 = false;
            boolean error2 = false;
            buffer.append("\"");
            nextCh();
            while (ch != '"' && ch != '\n' && ch != EOFCH) { 
                if (ch == '\\') // Escape starts here
                {			
                    nextCh();
                    if(ch == 'u') // Unicode Escape starts here
                    {
                    	isUni2 = true;
                    	buffer.append('\\');
                    	buffer.append(ch);
                    	nextCh();
                    	while(ch == 'u') // Append as many u's availabe and
                    	{		 // must follow four Hex-digit
                    		buffer.append(ch);
                    		nextCh();
                    	}
                    	for(int i = 0 ; i < 4; i++)
                    	{
                    		if(isDigit(ch) || (ch >= 'a' && ch <= 'f') || 
                				  (ch >= 'A' && ch <= 'F'))
                    		{
                    			buffer.append(ch);
                    			nextCh();
                    		}
                    		else //Error in case of any non-hex-digit
                    		{
                    			while(ch != '\"' && ch != ';' 
					      && ch != '\n' && ch != EOFCH)
                    			{
                    				buffer.append(ch);
                    				nextCh();
                    			}
                    			buffer.append(ch);
                    			reportScannerError
					    ("Badly formed Unicode escape. %s",
                    					buffer.toString());
                    			nextCh();
                    			return getNextToken();
                    		}
                    	}
                    }	
                    else if(ch >= '0' && ch <= '7') // Octal escape starts here
                    {				    
                    	buffer.append('\\');	    
                    	buffer.append(ch);	 
                    	nextCh();		    
                    	while(isDigit(ch))	  
                    	{		    
                    		buffer.append(ch);
                    		nextCh();
                    	}
                    }
                    else
                    {
                    	
                    	String temp1 = escape();
                    	if(temp1.length() != 0)
                    		buffer.append(temp1);
                    	else
                    	{
                    		while(ch != '\"' && ch != ';' 
				      && ch != '\n' && ch != EOFCH)
                    		{
                    			buffer.append(ch);
                    			nextCh();
                    		}
                    		nextCh();
                    		return getNextToken();
                    	}
                    }
                } 
                else 
                {
                    buffer.append(ch);
                    nextCh();
                }
                // Here we check if the Unicode is resulting in LF | CR | \ | "
                if(isUni2 && 	// we through an error and eats up whole string
                	  ((buffer.substring(buffer.length()-4,
                	    buffer.length()).equalsIgnoreCase("000a")) ||
                	   (buffer.substring(buffer.length()-4,
                	    buffer.length()).equalsIgnoreCase("000d")) ||
                	   (buffer.substring(buffer.length()-4,
                            buffer.length()).equalsIgnoreCase("005c")) ||
                           (buffer.substring(buffer.length()-4,
                            buffer.length()).equalsIgnoreCase("0022"))))
                {
                	while(ch != '\"' && ch != ';' && ch != '\n' 
			      && ch != EOFCH)
        			{
        				buffer.append(ch);
        				nextCh();
        			}
        			buffer.append(ch);
                	reportScannerError
			    ("Not Valid char literal. %s", buffer.toString());
                	nextCh();
                	return getNextToken();
                }
                if(isUni2 == true)
                	isUni2 = false;
            }
            
            if (ch == '\n') {
                reportScannerError("Unexpected end of line found in String");
            } else if (ch == EOFCH) {
                reportScannerError("Unexpected end of file found in String");
            } else {
                // Scan the closing "
                nextCh();
                buffer.append("\"");
            }
            return new TokenInfo(STRING_LITERAL, buffer.toString(),
                line);
        case '.':
            nextCh();
            buffer = new StringBuffer();
            boolean validF = false;
            if(isDigit(ch)) // . (0-9){0-9} after this it must be 
            {				//              float or double
            	buffer.append('.');
            	// call this function to read all input
            	// .(0-9){0-9}[(e|E)[+|-](0-9){0-9}][f|F|d|D| ]
            	buffer = foundPoint(buffer);	
            	return decider(buffer); //This decide the type or error
            }
            else
            	return new TokenInfo(DOT, line);
        case EOFCH:
            return new TokenInfo(EOF, line);
        case '0':
            // 0 can follow by x, ., hexdigit(0-7), digit 8 or 9, (f|F),
            // (d|D), (l|L) or nothing (0 as integer)
            buffer = new StringBuffer();  
            buffer.append(ch);
            nextCh();
            if(ch == 'x' || ch == 'X') // 0 (x|X) can follow hexdigit
            			       // or a (.)
            {
            	boolean validInt = false;
            	boolean validFloat = false;
            	buffer.append(ch);
            	nextCh();
            	while(isDigit(ch) || 
            			((ch >= 'a' && ch <= 'f') || 
            					(ch >= 'A' && ch <= 'F'))) 
            	{		          // 0(x|X)((0-9)|(a-f)|(A-F)){0-9}... 
            		validInt = true;  // can follow (l|L), (p|P)	 
            		buffer.append(ch);// or (.)
            		nextCh();
            	}
            	
            	if((ch == 'l' || ch == 'L') && validInt)  
            	{			  //0(x|X)((0-9)|(a-f)|(A-F)){0-9}(l|L)
            		nextCh();
            		return new TokenInfo(LONG_LITERAL, 
					     buffer.toString(), line);
            	}
            	else if((ch == 'p' || ch =='P') && validInt)  
            	{			   //0(x|X)((0-9)|(a-f)|(A-F)){0-9}(p|P)
            		buffer.append(ch); //must follow atleast one 
            		nextCh();	   //digit
            		errorfound = false;
            		// call this function to read all input
            		buffer = foundExponent(buffer);
            		return decider(buffer);  //This decide the type or error
            	}
            	//(0x|0X)(0-9)|(a-f)|(A-F){(0-9)|(a-f)|(A-F)} . 
		//must follow a (p|P)
            	else if(ch == '.' && validInt)
            	{
            		buffer.append(ch);
            		nextCh();
            		while(isDigit(ch) || 
                			((ch >= 'a' && ch <= 'f') || 
                			 (ch >= 'A' && ch <= 'F')))
                	{
                		buffer.append(ch);
                		nextCh();
                	}
            		if(ch == 'p' || ch =='P')
                	{
            			buffer.append(ch);   //must follow atleast one 
                		nextCh();	     //digit
                		errorfound = false;
                		// call this function to read all input
                		buffer = foundExponent(buffer);
                		return decider(buffer);
                	}
            		else 
            		{
            			//(0x|0X)(0-9)|(a-f)|(A-F){(0-9)|(a-f)|(A-F)} . 
			        //not follows (p|P)
            			reportScannerError
				    ("Not valid float/double digit."); 
				return getNextToken(); 
            		}
            			
            	}
            	//(0x|0X) . must follow (0-9)|(a-f)|(A-F)
            	else if(ch == '.' && !validInt)
            	{
            		boolean valid1 = false;
            		buffer.append(ch);
            		nextCh();
            		while(isDigit(ch) || 
                			((ch >= 'a' && ch <= 'f') || 
                			 (ch >= 'A' && ch <= 'F')))
                	{
            			//(0x|0X).(0-9)|(a-f)|(A-F){(0-9)|(a-f)|(A-F)} 
			        //must follow a (p|P)
            			valid1 = true;
                		buffer.append(ch);
                		nextCh();
                	}
            		if((ch == 'p' || ch =='P') && valid1)
                	{
            		//(0x|0X).(0-9)|(a-f)|(A-F){(0-9)|(a-f)|(A-F)}(p|P) 
            			buffer.append(ch);    //must follow atleast one 
                		nextCh();	      //digit
                		errorfound = false;
                		// call this function to read all input
                		buffer = foundExponent(buffer);
                		return decider(buffer);	
                	}
            		else 
            		{
            			//(0x|0X).(0-9)|(a-f)|(A-F){(0-9)|(a-f)|(A-F)}
            			// not followed by a (p|P)
            			reportScannerError
				    ("Not valid float/double digit."); 
				return getNextToken(); 
            		}
            			
            	}
            	else if(validInt)
            		// 0(x|X)((0-9)|(a-f)|(A-F)){0-9} is Int
            		return new TokenInfo(INT_LITERAL, 
					     buffer.toString(), line);
            	else 
        		{
            		//0(x|X) not followed by anything
            		if(!isWhitespace(ch))
            			nextCh();
        			reportScannerError("Not valid Hex digit."); 
                    return getNextToken(); 
        		}
            }
            // 0 ...
            else if(ch >= '0' && ch <= '7')
            {
            	do
            	{
            	buffer.append(ch);
            	nextCh();
            	}while(ch >= '0' && ch <= '7'); //0(0-7){0-7} can follow (f|F),
            					//(d|D),(l|L),(e|E), (8|9),(.)
            	if(ch == 'f' || ch == 'F')      // or nothing
            	{
            		//0(0-7){0-7}(f|F)
            		nextCh();
            		return new TokenInfo(FLOAT_LITERAL, 
					     buffer.toString(), line);
            	}
            	else if(ch == 'd' || ch == 'D') //0(0-7){0-7}(d|D)
            	{
            		nextCh();
            		return new TokenInfo(DOUBLE_LITERAL, 
					     buffer.toString(), line);
            	}
            	else if(ch == 'l' || ch == 'L') //0(0-7){0-7}(l|L)
            	{            		
            		nextCh();
            		return new TokenInfo(LONG_LITERAL, 
					     buffer.toString(), line);
            	}
            	else if(ch == 'e' || ch =='E') //0(0-9){0-9}(e|E) 
		    {
            		buffer.append(ch);     //must follow atleast one 
            		nextCh();	       //digit
            		errorfound = false;
            		// call this function to read all input
            		buffer = foundExponent(buffer);
            		return decider(buffer);	 //This decide the type or error
            	}
            	else if(ch == '8' || ch == '9') // 0 (0-9){0-9} can follow (.),
		    {                           //(e|E)
            		buffer.append(ch);
            		nextCh();
            		while(isDigit(ch))
            		{
            			buffer.append(ch);
            			nextCh();
            		}
            		if(ch == '.') // 0 (0-9){0-9} . can follow (e|E)
                	{
                		buffer.append(ch);
                		nextCh();
                		// call this function to read all input
                		buffer = foundPoint(buffer);
                		return decider(buffer);	  
                    }
            		else if(ch == 'e' || ch =='E') // 0 (0-9){0-9}(e|E) 
                	{
            			buffer.append(ch);     //must follow atleast 
                		nextCh();	       //one digit
                		errorfound = false;
                		// call this function to read all input
                		buffer = foundExponent(buffer);
                		return decider(buffer);	
                	}
            		else if(ch == 'f' || ch == 'F') // 0 (0-9){0-9}[f|F]
            		{
            			nextCh();
            			return new TokenInfo(FLOAT_LITERAL, 
						     buffer.toString(), line);
            		}
            		else if(ch == 'd' || ch == 'D') // 0 (0-9){0-9}[d|D]
            		{
            			nextCh();
            			return new TokenInfo(DOUBLE_LITERAL, 
						     buffer.toString(), line);
            		}
            		else // 0 (0-9){0-9} 8 or 9 are not ocatal digit
            		{
            			reportScannerError("Not valid octal digit."); 
				return getNextToken(); 
            		}
            		
            		
            	}
            	//0(0-7){0-7} . same as 0(0-9){0-9}. can follow (e|E)
            	else if(ch == '.')
            	{
            		buffer.append(ch);
            		nextCh();
            		// call this function to read all input
            		buffer = foundPoint(buffer);
            		return decider(buffer);	 //This decide the type or error
                }
            	else //0 (0-7){0-7} octal Integer 
            		return new TokenInfo(INT_LITERAL, 
					     buffer.toString(), line);
            }
            else if(ch == '8' || ch == '9') //0(8|9) can follow {0-9},(.),(e|E)
        	{
        		buffer.append(ch);
        		nextCh();
        		while(isDigit(ch)) //0 (8|9){0-9}
        		{
        			buffer.append(ch);
        			nextCh();
        		}
                        if(ch == '.') //0 (8|9){0-9} . can follow (e|E)
			{
        			buffer.append(ch);
				nextCh();
				// call this function to read all input
				buffer = foundPoint(buffer);
				return decider(buffer);	
			}
        		//0 (8|9){0-9}(e|E) must follow a digit
        		else if(ch == 'e' || ch =='E')
			{
        			buffer.append(ch);    //must follow atleast one 
				nextCh();	      //digit
				errorfound = false;
				// call this function to read all input
				buffer = foundExponent(buffer);
				return decider(buffer);	 
			}
        		else if(ch == 'f' || ch == 'F')
        		{
        			//0 (8|9)(f|F)
        			nextCh();
        			return new TokenInfo(FLOAT_LITERAL, 
						     buffer.toString(), line);
        		}
        		else if(ch == 'd' || ch == 'D')
        		{
        			//0 (8|9)(d|D)
        			nextCh();
        			return new TokenInfo(DOUBLE_LITERAL, 
						     buffer.toString(), line);
        		}
        		else
        		{
        			//0(8|9) not valid octal number
        			reportScannerError("Not valid octal number."); 
				return getNextToken(); 
        		}
        		
        	}
         // 0 . can follow {0-9}, [d|D], [f|F], [e|e]
            else if(ch == '.')
            {
            	buffer.append(ch);
        		nextCh();
        		// call this function to read all input
        		buffer = foundPoint(buffer);
        		return decider(buffer);	 //This decide the type or error
            }
            else if(ch == 'e' || ch =='E') //0 (e|E) must follow a digit
        	{
            	buffer.append(ch);		    //must follow atleast one 
        		nextCh();		    //digit
        		errorfound = false;
        		// call this function to read all input
        		buffer = foundExponent(buffer);
        		return decider(buffer);	 //This decide the type or error
        	}
            else if(ch == 'l' || ch == 'L') // 0 (l|L)
            	{
            		nextCh();
            		return new TokenInfo(LONG_LITERAL, 
					     buffer.toString(), line);
            	}
            else if (ch == 'f' || ch == 'F') // 0 (f|F)
                {
                    nextCh();
                    return new TokenInfo(FLOAT_LITERAL, 
					 buffer.toString(), line);
                }
            else if (ch == 'd' || ch == 'D') // 0 (d|D)
            	{
                    nextCh();
                    return new TokenInfo(DOUBLE_LITERAL, 
					 buffer.toString(), line);
            	}
            else // 0 followed by nothing
            	return new TokenInfo(INT_LITERAL, "0", line);
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
        	// (0-9)... can follow by {0-9}, (.), (e|E), (l|L), (d|D), (f|F)
            buffer = new StringBuffer();
            buffer.append(ch);
            nextCh();
            while (isDigit(ch)) //(0-9){0-9}
            {
                buffer.append(ch);
                nextCh();
            }
            if(ch == '.') //(0-9){0-9} . 
            {
            	        buffer.append(ch);
        		nextCh();
        		// call this function to read all input
        		buffer = foundPoint(buffer);
        		return decider(buffer);	 //This decide the type or error
            }
          //(0-9){0-9}(e|E) must follow a digit
            else if(ch == 'e' || ch =='E')
        	{
            	buffer.append(ch);		     //must follow atleast one 
        		nextCh();		     //digit
        		errorfound = false;
        		// call this function to read all input
        		buffer = foundExponent(buffer);
        		return decider(buffer);	 //This decide the type or error
        	}
            if(ch == 'l' || ch == 'L') //(0-9){0-9}(l|L)
            {
            	nextCh();
            	return new TokenInfo(LONG_LITERAL, buffer.toString(), line);
            }
            else if(ch == 'd' || ch == 'D') //(0-9){0-9}(d|D)
            {
            	nextCh();
            	return new TokenInfo(DOUBLE_LITERAL, buffer.toString(), line);
            }
            else if(ch == 'f' || ch == 'F') //(0-9){0-9}(f|F)
            {
            	nextCh();
            	return new TokenInfo(FLOAT_LITERAL, buffer.toString(), line);
            }
            else //(0-9){0-9}
            	return new TokenInfo(INT_LITERAL, buffer.toString(), line);
            
        default:
            if (isIdentifierStart(ch)) {
                buffer = new StringBuffer();
                while (isIdentifierPart(ch)) {
                    buffer.append(ch);
                    nextCh();
                }
                String identifier = buffer.toString();
                if (reserved.containsKey(identifier)) {
                    return new TokenInfo(reserved.get(identifier),
                        line);
                } else {
                    return new TokenInfo(IDENTIFIER, identifier, line);
                }
            } else {
                reportScannerError("Unidentified input token: '%c'", ch);
                nextCh();
                return getNextToken();
            }
        }
    }

    /**
     * Scan and return an escaped character.
     * 
     * @return escaped character.
     */

    private String escape() {
        switch (ch) {
        case 'b':
            nextCh();
            return "\\b";
        case 't':
            nextCh();
            return "\\t";
        case 'n':
            nextCh();
            return "\\n";
        case 'f':
            nextCh();
            return "\\f";
        case 'r':
            nextCh();
            return "\\r";
        case '"':
            nextCh();
            return "\"";
        case '\'':
            nextCh();
            return "\\'";
        case '\\':
            nextCh();
            return "\\\\";
        default:
            reportScannerError("Badly formed escape: \\%c", ch);
            nextCh();
            return "";
        }
    }

    /**
     * Advance ch to the next character from input, and update
     * the line number.
     */

    private void nextCh() {
        line = input.line();
        try {
            ch = input.nextChar();
        } catch (Exception e) {
            reportScannerError("Unable to read characters from input");
        }
    }

    /**
     * Report a lexcial error and record the fact that an error
     * has occured. This fact can be ascertained from the Scanner
     * by sending it an errorHasOccurred() message.
     * 
     * @param message
     *                message identifying the error.
     * @param args
     *                related values.
     */

    private void reportScannerError(String message, Object... args) {
        isInError = true;
        System.err.printf("%s:%d: ", fileName, line);
        System.err.printf(message, args);
        System.err.println();
    }

    /**
     * Return true if the specified character is a digit (0-9);
     * false otherwise.
     * 
     * @param c
     *                character.
     * @return true or false.
     */

    private boolean isDigit(char c) {
        return (c >= '0' && c <= '9');
    }

    /**
     * Return true if the specified character is a whitespace;
     * false otherwise.
     * 
     * @param c
     *                character.
     * @return true or false.
     */

    private boolean isWhitespace(char c) {
        switch (c) {
        case ' ':
        case '\t':
        case '\n': // CharReader maps all new lines to '\n'
        case '\f':
            return true;
        }
        return false;
    }

    /**
     * Return true if the specified character can start an
     * identifier name; false otherwise.
     * 
     * @param c
     *                character.
     * @return true or false.
     */

    private boolean isIdentifierStart(char c) {
        return (c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z'
            || c == '_' || c == '$');
    }

    /**
     * Return true if the specified character can be part of an
     * identifier name; false otherwise.
     * 
     * @param c
     *                character.
     * @return true or false.
     */

    private boolean isIdentifierPart(char c) {
        return (isIdentifierStart(c) || isDigit(c));
    }

    /**
     * Has an error occurred up to now in lexical analysis?
     * 
     * @return true or false.
     */

    public boolean errorHasOccurred() {
        return isInError;
    }

    /**
     * The name of the source file.
     * 
     * @return name of the source file.
     */

    public String fileName() {
        return fileName;
    }
    /**
     * This function is called after an (e|E) is found.
     * 
     * @return StringBuffer.
     */
    private StringBuffer foundExponent(StringBuffer buffer)
    {
		if(ch == '+' || ch == '-')     //optional [+|-]
		{
			buffer.append(ch);
			nextCh();
		}
		if(!isDigit(ch))	 //sets errorfound flag to true if (e|E)
			errorfound = true;//is not followed by atleast 1 digit	
		while(isDigit(ch))
		{
			buffer.append(ch);
			nextCh();
		}
		return buffer;		      //return buffer with all input
    }
    /**
     * This function is called after a (.) is found.
     * 
     * @return StringBuffer.
     */
    private StringBuffer foundPoint(StringBuffer buffer)
    {
    	while(isDigit(ch))		//can have many digits after a (.)
	{
		buffer.append(ch);
	        nextCh();
	}
    	if(ch == 'e' || ch == 'E')	//After reading (e|E) simply calls
    	{				//foundExponent function
    		buffer.append(ch);
    		nextCh();
    		errorfound = false;
    		buffer = foundExponent(buffer);
    	}
		
		return buffer;		//return buffer with all input
    }
    /**
     * This function is called afterdone reading all input.
     * it may report error depending on errorfound flag
     * @return TokenInfo.
     */
    private TokenInfo decider(StringBuffer buffer)
    {
	//some grammar followed by [d|D]
    	if((ch == 'd' || ch == 'D') && !errorfound) 
		{
			nextCh();
			return new TokenInfo(DOUBLE_LITERAL, 
					     buffer.toString(), line);
		}
		else if((ch == 'f' || ch == 'F') && !errorfound)  
		{                           //some grammar followed by [f|F] 
			nextCh();
			return new TokenInfo(FLOAT_LITERAL, 
					     buffer.toString(), line);
		}
		else if(!errorfound)	//some grammar followed by no suffix
			return new TokenInfo(DOUBLE_LITERAL, 
					     buffer.toString(), line);
		else 
		{
			//In other case it is always an error and reports it
		        //and move further
		        reportScannerError("Not valid float/double number."); 
			return getNextToken(); 
		}
    }
    
    
}

/**
 * A buffered character reader. Abstracts out differences between
 * platforms, mapping all new lines to '\n'. Also, keeps track of
 * line numbers where the first line is numbered 1.
 */

class CharReader {

    /** A representation of the end of file as a character. */
    public final static char EOFCH = (char) -1;

    /** The underlying reader records line numbers. */
    private LineNumberReader lineNumberReader;

    /** Name of the file that is being read. */
    private String fileName;

    /**
     * Construct a CharReader from a file name.
     * 
     * @param fileName
     *                the name of the input file.
     * @exception FileNotFoundException
     *                    if the file is not found.
     */

    public CharReader(String fileName) throws FileNotFoundException {
        lineNumberReader = new LineNumberReader(new FileReader(
            fileName));
        this.fileName = fileName;
    }

    /**
     * Scan the next character.
     * 
     * @return the character scanned.
     * @exception IOException
     *                    if an I/O error occurs.
     */

    public char nextChar() throws IOException {
        return (char) lineNumberReader.read();
    }

    /**
     * The current line number in the source file, starting at 1.
     * 
     * @return the current line number.
     */

    public int line() {
        // LineNumberReader counts lines from 0.
        return lineNumberReader.getLineNumber() + 1;
    }

    /**
     * Return the file name.
     * 
     * @return the file name.
     */

    public String fileName() {
        return fileName;
    }

    /**
     * Close the file.
     * 
     * @exception IOException
     *                    if an I/O error occurs.
     */

    public void close() throws IOException {
        lineNumberReader.close();
    }
}
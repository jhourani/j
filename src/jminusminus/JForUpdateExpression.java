package jminusminus;

import static jminusminus.CLConstants.*;
import static jminusminus.TokenKind.SEMI;

import java.util.ArrayList;

/**
 * The AST node for a forUpdate expression.
 */

class JForUpdateExpression extends JExpression {

	private ArrayList<JStatement> statementExpressions;

    /**
     * Construct an AST node for a forUpdate expression.
     * 
     * @param line
     *            line in which the forUpdate expr occurs in the source file.
     * @param statementExpressions
     * 			  the 0 or more statementExpressions
     * 			   
     */

    public JForUpdateExpression(int line,
    		ArrayList<JStatement> statementExpressions) {
    	super(line);
    	this.statementExpressions = statementExpressions;
    }

    /**
     * TODO
     */

    public JExpression analyze(Context context) {
        return null;
    }

    /**
     * TODO
     */

    public void codegen(CLEmitter output) {
        ;
    }

    /**
     * @inheritDoc
     */

    public void writeToStdOut(PrettyPrinter p) {
    	 p.printf("<JForUpdateStatement line=\"%d\">\n", line());
         p.printf("</JForUpdateStatement>\n");
    }

}

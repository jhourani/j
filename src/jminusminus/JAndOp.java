package jminusminus;

import java.util.ArrayList;

/**
 * The AST node for an AND ( & ) expression. It keeps track of its
 * type, the Constructor representing the expression, its arguments and their
 * types.
 */

class JAndOp extends JExpression {

    /** The arguments to the constructor. */
    protected ArrayList<JExpression> rhs_list;

    protected JExpression lhs;
    
    protected String operator; 

    /**
     * Construct an AST node for an AND ( & ) expression.
     * 
     * @param line
     *            the line in which the AND expression occurs in the source
     *            file.
     * @param type
     *            the type being constructed.
     * @param arguments
     *            arguments to the constructor.
     */

    public JAndOp(int line, String operator, JExpression lhs,
    		ArrayList<JExpression> arguments) {
        super(line);
        this.operator = operator;
        this.lhs = lhs;
        this.rhs_list = arguments;
    }

    /**
     * TODO
     */
    public JExpression analyze(Context context) {
        return null;
    }
    
    /**
     * TODO
     */
    public void codegen(CLEmitter output) {
        
    }

    /**
     * @inheritDoc
     */

    public void writeToStdOut(PrettyPrinter p) {
        p.printf("<JAndOp line=\"%d\" type=\"%s\"/>\n", line(),
                ((type == null) ? "" : type.toString()));
        p.indentRight();
        p.println("<lhs>");
        p.indentRight();
        lhs.writeToStdOut(p);
        p.indentLeft();
        p.println("</lhs>");
        if (rhs_list != null) {
            for (JExpression argument : rhs_list) {
                p.println("<rhs>");
                p.indentRight();
                argument.writeToStdOut(p);
                p.indentLeft();
                p.println("</rhs>");
                p.indentLeft();
            }
        }
        p.indentLeft();
        p.println("</JAndOp>");
    }

}

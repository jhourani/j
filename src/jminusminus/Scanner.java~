// Copyright 2011 Bill Campbell, Swami Iyer and Bahar Akbal-Delibas

package jminusminus;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Hashtable;
import static jminusminus.TokenKind.*;

/**
 * A lexical analyzer for j--, that has no backtracking
 * mechanism.
 * 
 * When you add a new token to the scanner, you must also add an
 * entry in the TokenKind enum in TokenInfo.java specifying the
 * kind and image of the new token.
 */

class Scanner {

    /** End of file character. */
    public final static char EOFCH = CharReader.EOFCH;

    /** Keywords in j--. */
    private Hashtable<String, TokenKind> reserved;

    /** Source characters. */
    private CharReader input;

    /** Next unscanned character. */
    private char ch;

    /** Whether a scanner error has been found. */
    private boolean isInError;

    /** Source file name. */
    private String fileName;

    /** Line number of current token. */
    private int line;
    
    private boolean isFloat = true;

    /**
     * Construct a Scanner object.
     * 
     * @param fileName
     *                the name of the file containing the source.
     * @exception FileNotFoundException
     *                    when the named file cannot be found.
     */

    public Scanner(String fileName) throws FileNotFoundException {
        this.input = new CharReader(fileName);
        this.fileName = fileName;
        isInError = false;

        // Keywords in j--
        reserved = new Hashtable<String, TokenKind>();
        reserved.put(ABSTRACT.image(), ABSTRACT);
        reserved.put(BOOLEAN.image(), BOOLEAN);
        reserved.put(CHAR.image(), CHAR);
        reserved.put(CLASS.image(), CLASS);
        reserved.put(ELSE.image(), ELSE);
        reserved.put(EXTENDS.image(), EXTENDS);
        reserved.put(FALSE.image(), FALSE);
        reserved.put(IF.image(), IF);
        reserved.put(IMPORT.image(), IMPORT);
        reserved.put(INSTANCEOF.image(), INSTANCEOF);
        reserved.put(INT.image(), INT);
        reserved.put(NEW.image(), NEW);
        reserved.put(NULL.image(), NULL);
        reserved.put(PACKAGE.image(), PACKAGE);
        reserved.put(PRIVATE.image(), PRIVATE);
        reserved.put(PROTECTED.image(), PROTECTED);
        reserved.put(PUBLIC.image(), PUBLIC);
        reserved.put(RETURN.image(), RETURN);
        reserved.put(STATIC.image(), STATIC);
        reserved.put(SUPER.image(), SUPER);
        reserved.put(THIS.image(), THIS);
        reserved.put(TRUE.image(), TRUE);
        reserved.put(VOID.image(), VOID);
        reserved.put(WHILE.image(), WHILE);
        //p2
        reserved.put(BREAK.image(),BREAK ); 
        reserved.put(BYTE.image(), BYTE);
        reserved.put(CASE.image(), CASE); 
        reserved.put(CATCH.image(), CATCH); 
        reserved.put(CONST.image(), CONST); 
        reserved.put(CONTINUE.image(), CONTINUE);
        reserved.put(DEFAULT.image(), DEFAULT);
        reserved.put(DO.image(), DO); 
        reserved.put(DOUBLE.image(), DOUBLE); 
        reserved.put(FINAL.image(), FINAL);
        reserved.put(FINALLY.image(), FINALLY); 
        reserved.put(FLOAT.image(), FLOAT); 
        reserved.put(FOR.image(), FOR); 
        reserved.put(GOTO.image(), GOTO); 
        reserved.put(IMPLEMENTS.image(), IMPLEMENTS); 
        reserved.put(INTERFACE.image(), INTERFACE); 
        reserved.put(LONG.image(), LONG); 
        reserved.put(NATIVE.image(), NATIVE);
        reserved.put(STRICTFP.image(), STRICTFP); 
        reserved.put(SYNCHRONIZED.image(), SYNCHRONIZED); 
        reserved.put(THROW.image(), THROW); 
        reserved.put(THROWS.image(), THROWS);
        reserved.put(TRANSIENT.image(), TRANSIENT); 
        reserved.put(TRY.image(), TRY); 
        reserved.put(VOLATILE.image(), VOLATILE);
        reserved.put(ASSERT.image(), ASSERT);
		reserved.put(ENUM.image(), ENUM);
        
        
        
        
        
        

        // Prime the pump.
        nextCh();
    }

    /**
     * Scan the next token from input.
     * 
     * @return the the next scanned token.
     */

    public TokenInfo getNextToken() {
        StringBuffer buffer;
        boolean moreWhiteSpace = true;
        while (moreWhiteSpace) {
            while (isWhitespace(ch)) {
                nextCh();
            }
            if (ch == '/') {
                nextCh();
                if (ch == '/') {
                    // CharReader maps all new lines to '\n'
                    while (ch != '\n' && ch != EOFCH) {
                        nextCh();
                    }
                } 
                else if (ch == '*')
                {
                	nextCh();
                	char previousCh;
                	while (ch != EOFCH)
                	{
                		previousCh = ch;
                		nextCh();
                		if (previousCh == '*' && ch == '/')
                			break;	
                	}
                	nextCh();
                } 
                else if ( ch == '='){
                	nextCh();
                	return new TokenInfo (DIV_ASSIGN, line);
                }
                else {
                	 return new TokenInfo(DIV, line);
                    //reportScannerError("Operator / is not supported in j--.");
                }
            } 
            else {
                moreWhiteSpace = false;
            }
        }
        line = input.line();
        switch (ch) {
        case '(':
            nextCh();
            return new TokenInfo(LPAREN, line);
        case ')':
            nextCh();
            return new TokenInfo(RPAREN, line);
        case '{':
            nextCh();
            return new TokenInfo(LCURLY, line);
        case '}':
            nextCh();
            return new TokenInfo(RCURLY, line);
        case '[':
            nextCh();
            return new TokenInfo(LBRACK, line);
        case ']':
            nextCh();
            return new TokenInfo(RBRACK, line);
        case ';':
            nextCh();
            return new TokenInfo(SEMI, line);
        case ':':
			nextCh();
			return new TokenInfo(COLON, line);
        case ',':
            nextCh();
            return new TokenInfo(COMMA, line);
        case '?':
            nextCh();
            return new TokenInfo(QUESTION, line);
        case '=':
            nextCh();
            if (ch == '=') {
                nextCh();
                return new TokenInfo(EQUAL, line);
            } else {
                return new TokenInfo(ASSIGN, line);
            }
        case '!':
            nextCh();
            if (ch == '='){
            	nextCh();
            	return new TokenInfo(NOT_EQUAL, line);            	
            }
            else{
               return new TokenInfo(LNOT, line);
            }
        case '~':
            nextCh();
            return new TokenInfo(NOT, line);    
        case '*':
            nextCh();
            if(ch == '='){
            	nextCh();
                return new TokenInfo(STAR_ASSIGN, line);
            }
            else{
                return new TokenInfo(STAR, line);
            }
        case '+':
            nextCh();
            if (ch == '=') {
                nextCh();
                return new TokenInfo(PLUS_ASSIGN, line);
            } else if (ch == '+') {
                nextCh();
                return new TokenInfo(INC, line);
            } else {
                return new TokenInfo(PLUS, line);
            }
        case '-':
            nextCh();
            if (ch == '-') {
                nextCh();
                return new TokenInfo(DEC, line);
            } 
            else if (ch == '='){
            	nextCh();
            	return new TokenInfo(MINUS_ASSIGN, line);
            }
            else {
                return new TokenInfo(MINUS, line);
            }
        case '%':
            nextCh();
            if (ch == '=') {
                nextCh();
                return new TokenInfo(MOD_ASSIGN, line);
            } else {
                return new TokenInfo(MOD, line);
            }
            
                    
        case '|':
            nextCh();
             {
            	 if (ch == '='){
            		 nextCh();
            		 return new TokenInfo(BOR_ASSIGN, line);          		 
            	 }
            	 else if (ch == '|') {
            		 nextCh();
            		 return new TokenInfo(OR, line);
            		 
            	 }
            	else{
                     return new TokenInfo(BOR, line);
            	}
             }
            
        case '^':
            nextCh();
             {
            	 if (ch == '='){
            		 nextCh();
            		 return new TokenInfo(BXOR_ASSIGN, line);
            	 }
            	 else{
                    return new TokenInfo(BXOR, line);
            	 }
            }
            
      case '&':
            nextCh();
            if (ch == '&') {
                nextCh();
                return new TokenInfo(BAND, line);
            }
            else if (ch == '=') {
                nextCh();
                return new TokenInfo(BAND_ASSIGN, line);
            }
            else {
            	return new TokenInfo(BAND, line);
            }
      case '>':
          nextCh();
          if (ch == '>') {
          	nextCh();
          	if (ch == '>') {
          		nextCh();
          		if (ch == '=') {
          			nextCh();
          			return new TokenInfo(BSR_ASSIGN, line);
          		}
          	return new TokenInfo(UBSR, line);
          }
          else if (ch == '='){
          	nextCh();
          	return new TokenInfo(SR_ASSIGN, line);  
          }
          	return new TokenInfo(BSR, line);                   
          }
          else if (ch == '=') {
              nextCh();
              return new TokenInfo(GE, line);
          } 
          else{
               return new TokenInfo(GT, line);
          }
      case '<':
          nextCh();
          if (ch == '=') {
              nextCh();
              return new TokenInfo(LE, line);
          }
          // added signed left shift for homework #2
          else if (ch == '<') {
          	nextCh();
          	if (ch == '=') {
          		nextCh();
          		return new TokenInfo(SL_ASSIGN, line);
          	}
          	else
          		return new TokenInfo(SLS, line);
          }
          // added less than for homework #2
          else 
              return new TokenInfo(LT, line);
        case '\'':
            buffer = new StringBuffer();
            buffer.append('\'');
            nextCh();
            if (ch == '\\') {
                nextCh();
                //unicode char
                if (ch == 'u') {
                	while(ch == 'u'){ 
    					nextCh();
                	}					
					buffer.append(unicode('c'));
				} else {
					buffer.append(escape());
				}
            }
           
            else if (ch == '\'')
			{
				buffer.append(ch);
				reportScannerError("Empty character literal.  %s", buffer.toString() );
				nextCh();
				return getNextToken(); 
			}
            else if (ch == '\n' || ch == EOFCH)
				{
					reportScannerError("illegal line end in character literal" );
					nextCh();
					return getNextToken(); 

				}
            
            else {
                buffer.append(ch);
                nextCh();
            }
            if (ch == '\'') {
                buffer.append('\'');
                nextCh();
                return new TokenInfo(CHAR_LITERAL, buffer.toString(),
                    line);
            } else {
                // Expected a ' ; report error and try to
                // recover.
                reportScannerError(ch
                    + " found by scanner where closing ' was expected.");
                while (ch != '\'' && ch != ';' && ch != '\n') {
                    nextCh();
                }
              
            }
    
        case '"':
			buffer = new StringBuffer();
			buffer.append("\"");
			nextCh();
			while (ch != '"' && ch != '\n' && ch != EOFCH) {
				if (ch == '\\') {
					nextCh();
					//unicode string
					if (ch == 'u') {
						while(ch == 'u'){ 
	    					nextCh();
	                	}
						buffer.append(unicode('s'));
					} 
					else {
						buffer.append(escape());
					}
				}
				
				else {
					buffer.append(ch);
					nextCh();
				}
			}

			if ((ch == '\n' ) || (ch == EOFCH)) {
				reportScannerError("Unexpected end of line found in String");
				nextCh();
				return getNextToken();
			
			}
			
            else {
				// Scan the closing "
				nextCh();
				buffer.append("\"");
				return new TokenInfo(STRING_LITERAL, buffer.toString(), line);
			}
			
        
        case '.':
        	nextCh();
        	if (isDigit(ch))        		
			{ 
        		buffer = new StringBuffer();
                buffer.append('.');
				//. (0-9){0-9} [(e | E) [+ | -] (0-9) {0-9}]
				buffer = scanFloat(buffer);
				if (isFloat == false)
				{
					isFloat=true;
					return getNextToken(); 
				}
				 //. (0-9){0-9} [(e | E) [+ | -] (0-9) {0-9}] [d | D | f | F]
				//float
				if ((ch == 'f') || (ch == 'F'))
				{
					buffer.append(ch);
					nextCh();
					return new TokenInfo(FLOAT_LITERAL, buffer.toString(), line);
				}
				//double
				else if ((ch == 'd') || (ch == 'D'))
					{
						buffer.append(ch);
						nextCh();
						return new TokenInfo(DOUBLE_LITERAL, buffer.toString(), line);
					}
			    else 
					{
						return new TokenInfo(DOUBLE_LITERAL, buffer.toString(), line);
					}
			}
			else 
				//.
				return new TokenInfo(DOT, line);
			
        case EOFCH:
            return new TokenInfo(EOF, line);
      
        
        case '0':
            nextCh();
            if (ch == 'x' ||  ch =='X'){
             buffer = new StringBuffer();
       	     buffer.append('0');
       	     buffer.append(ch);
             nextCh();
            	 if (isHexDigit(ch)){
            		 /** (0x | 0X)  (0-9) | (a-f) | (A-F) {(0-9) | (a-f) | (A-F)}
             	         [.{(0-9) | (a-f) | (A-F)}](p | P) [+ | -] (0-9){0-9} [d | D | f | F] **/
            	     while (isHexDigit(ch)) {            	    	 
            			   buffer.append(ch);
                           nextCh();
                           }
            	
            	      if (ch == 'l' || ch == 'L'){
            	           buffer.append(ch);
            	           nextCh();
            	           return new TokenInfo(LONG_LITERAL, buffer.toString(), line);
            	       } 
            	      else  if (ch =='.'){
            	        	 buffer.append(ch);
            	        	 nextCh();
            	        	 if (isHexDigit(ch)){
            	        		 while (isHexDigit(ch)){
            	        			 buffer.append(ch);
                    	        	 nextCh(); 
            	        		 }
            	        	 }
            	        	 if (ch == 'p' || ch =='P'){
            	        		 buffer.append(ch);
                	        	 nextCh();
                	        	 //take care of [+ | -] (0-9){0-9}
                	        	 buffer = scanDigit(buffer);
                	        	 if (isFloat == false){ 
                	        		         isFloat=true;
     										return getNextToken(); 
     									}
                	        	 if ((ch == 'f') || (ch == 'F'))
                					{
                						buffer.append(ch);
                						nextCh();
                						return new TokenInfo(FLOAT_LITERAL, buffer.toString(), line);
                					}
                					//double
                					else if ((ch == 'd') || (ch == 'D'))
                						{
                							buffer.append(ch);
                							nextCh();
                							return new TokenInfo(DOUBLE_LITERAL, buffer.toString(), line);
                						}
                				    else 
                						{
                							return new TokenInfo(DOUBLE_LITERAL, buffer.toString(), line);
                						}              	        	 
                	          	 }
            	        	//missing (p | P)
								else
								{
									reportScannerError("Not valid float literal. %s",buffer.toString()); 
									return getNextToken(); 
								}
            	         }
            	      else if (ch == 'p' || ch == 'P'){
            	    	  buffer.append(ch);
         	        	 nextCh();
         	        	 //take care of [+ | -] (0-9){0-9}
         	        	 buffer = scanDigit(buffer);
         	        	 if (isFloat == false){ 
         	        		         isFloat=true;
										return getNextToken(); 
									}
         	        	 if ((ch == 'f') || (ch == 'F'))
         					{
         						buffer.append(ch);
         						nextCh();
         						return new TokenInfo(FLOAT_LITERAL, buffer.toString(), line);
         					}
         					//double
         					else if ((ch == 'd') || (ch == 'D'))
         						{
         							buffer.append(ch);
         							nextCh();
         							return new TokenInfo(DOUBLE_LITERAL, buffer.toString(), line);
         						}
         				    else 
         						{
         							return new TokenInfo(DOUBLE_LITERAL, buffer.toString(), line);
         						}         
            	      }
                        	    
            		 /*hexidecimals
            		  *  0 (x | X) ((0-9) | (A-F) | (a-f)) {(0-9) | (A-F) | (a-f)
            		  */
            	    return new TokenInfo(INT_LITERAL, buffer.toString(), line); 
            	 }//end of 0X hex
            	 else if (ch =='.'){
            	/**| (0x | 0X) . (0-9) | (a-f) | (A-F) {(0-9) | (a-f) | (A-F)}
            	 * (p | P) [+ | -] (0-9){0-9} [d | D | f | F]
            	 */
            		 buffer.append(ch);
                     nextCh(); 
                     if (isHexDigit(ch)){
                    	 while(isHexDigit(ch)){
                    		 buffer.append(ch);
                             nextCh();  
                    	 }
                    	 if ((ch == 'p') || (ch == 'P'))
							{
								buffer.append(ch);
								nextCh();
								buffer = scanDigit(buffer);
								if (isFloat == false)
								{
									isFloat=true;
									return getNextToken(); 
								}
								
								//float
								if ((ch == 'f') || (ch == 'F'))
								{
									buffer.append(ch);
									nextCh();
									return new TokenInfo(FLOAT_LITERAL, buffer.toString(), line);
								}
								//double
								else if ((ch == 'd') || (ch == 'D'))
									{
										buffer.append(ch);
										nextCh();
										return new TokenInfo(DOUBLE_LITERAL, buffer.toString(), line);
									}
								else 
									{
										return new TokenInfo(DOUBLE_LITERAL, buffer.toString(), line);
									}
							}
                    	 //no p
                    	 else
							{
								reportScannerError("Not valid float literal. %s",buffer.toString()); 
								return getNextToken(); 
							}
						}
                     // no hex digit after 0X dot
                     else 
						{
                    	   	reportScannerError("Not valid float literal. %s",buffer.toString()); 
							return getNextToken(); 
						}
            	 }//end of case 0X dot
            	 /**
            	  * 0XL
            	  */
            	 else if (ch == 'l' || ch == 'L'){								
						buffer.append(ch);
						nextCh();
						reportScannerError("Not valid Long literal. %s",buffer.toString()); 
						return getNextToken(); 
					}
            	 /**
            	  * 0X
            	  */
            	 else {
            		 buffer.append(ch);
					 nextCh();
            		 reportScannerError("Not valid Hex number. %s",buffer.toString()); 
					return getNextToken();
            	 }
            	 
            
            }//end of 0X
            
            
            else if	(ch == '.')	{		
				/**
				 *  0 (0�9) {0�9} . {0-9} [(e | E) [+ | -] (0-9) {0-9}]
				 */
            	buffer = new StringBuffer();
      	        buffer.append('0');
      	        buffer.append(ch);
                nextCh();				
					if (isDigit(ch)){
						while(isDigit(ch)){
							buffer.append(ch);
							nextCh();
						}
						
					}
					if (ch == 'e'|| ch == 'E'){
						buffer.append(ch);
						nextCh();
						buffer = scanFloat (buffer);
					}
					
					//0 (0�9) {0�9} . {0-9} [(e | E) [+ | -] (0-9) {0-9}] [d | D | f | F]
					//float
					if ((ch == 'f') || (ch == 'F'))
					{
						buffer.append(ch);
						nextCh();
						return new TokenInfo(FLOAT_LITERAL, buffer.toString(), line);
					}
					//double
					else if ((ch == 'd') || (ch == 'D'))
						{
							buffer.append(ch);
							nextCh();
							return new TokenInfo(DOUBLE_LITERAL, buffer.toString(), line);
						}
				   else 
						{
							return new TokenInfo(DOUBLE_LITERAL, buffer.toString(), line);
						}
					
					
				}
                      
           else if (isOctal(ch)){
            	buffer = new StringBuffer();
            	buffer.append('0');
       		    while (isOctal(ch)){       		    	     		    	
       			       buffer.append(ch);
                       nextCh();
       		    	}
       		    
                    if (ch == 'l' || ch == 'L'){
          	    		 buffer.append(ch);
                         nextCh();
                    	 return new TokenInfo(LONG_LITERAL, buffer.toString(), line);
                         }
                    else if (ch == '8' || ch == '9') {
    					while (isDigit(ch)) {
    						buffer.append(ch);
    						nextCh();
    					}
    					if (ch == 'l' || ch == 'L'){
    						reportScannerError("Not valid long Octal number. %s",buffer.toString());
        					return getNextToken();
    					}
    					else{
    					reportScannerError("Not valid Octal number. %s",buffer.toString());
    					return getNextToken();
    					}
    				}
                    else  if (ch == 'd' || ch == 'D'){
          	    		 buffer.append(ch);
                         nextCh();
                    	 return new TokenInfo(DOUBLE_LITERAL, buffer.toString(), line);
                         }
                    else  if (ch == 'f' || ch == 'F'){
         	    		 buffer.append(ch);
                        nextCh();
                   	 return new TokenInfo(FLOAT_LITERAL, buffer.toString(), line);
                        }
                                          
        		   /*octals
        		    *  0 (0-7) {0-7}
        		    */
       	       return new TokenInfo(INT_LITERAL, buffer.toString(), line);
           }
           else if  (ch == '8' || ch == '9'){
        	   buffer = new StringBuffer();
        	   buffer.append('0');
        	   buffer.append(ch);
				while (isDigit(ch)) {
					buffer.append(ch);
					nextCh();
				}
				reportScannerError("Not valid Integer Literal. %s",buffer.toString());
				return getNextToken();
           }
           else if (ch == 'l' || ch == 'L'){
            	buffer = new StringBuffer();
            	buffer.append('0');
 	    		buffer.append(ch);
                nextCh();                   
                return new TokenInfo(LONG_LITERAL, buffer.toString(), line);
                   
                  
 	    	 }
           else if (ch == 'd' || ch == 'D'){
           	buffer = new StringBuffer();
           	buffer.append('0');
	    	buffer.append(ch);
               nextCh();                   
               return new TokenInfo(DOUBLE_LITERAL, buffer.toString(), line);                  
                 
	    	 }  
           else if (ch == 'f' || ch == 'F'){
              	buffer = new StringBuffer();
              	buffer.append('0');
   	    	buffer.append(ch);
                  nextCh();                   
                  return new TokenInfo(FLOAT_LITERAL, buffer.toString(), line);                  
                    
   	    	 }  
            
            
              return new TokenInfo(INT_LITERAL, "0", line);
            
            
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
        	buffer = new StringBuffer();
            while (isDigit(ch)) {
                buffer.append(ch);
                nextCh();
            }
            if (ch == '.'){
            	/**
            	 *  (0�9) {0�9} . {0-9} [(e | E) [+ | -] (0-9) {0-9}] [d | D | f | F]
            	 */
                buffer.append(ch);
                nextCh();            	
            	buffer = scanFloat(buffer);
            	if (isFloat == false)
				{
            		
					isFloat=true;
					return getNextToken(); 
				}
            	//float
				if ((ch == 'f') || (ch == 'F'))
				{
					buffer.append(ch);
					nextCh();
					return new TokenInfo(FLOAT_LITERAL, buffer.toString(), line);
				}
				//double
				else if ((ch == 'd') || (ch == 'D'))
					{
						buffer.append(ch);
						nextCh();
						return new TokenInfo(DOUBLE_LITERAL, buffer.toString(), line);
					}
			   else 
					{
						return new TokenInfo(DOUBLE_LITERAL, buffer.toString(), line);
					}
            	
            }
            else if (ch =='e' || ch == 'E'){
            	/**
            	 *  (0-9) {0-9} ((e | E) ([+ | -] (0-9) {0-9}) [d | D | f | F]
            	 */
            	buffer.append(ch);
                nextCh();            	
            	buffer = scanDigit(buffer);
            	if (isFloat == false)
				{
            		
					isFloat=true;
					return getNextToken(); 
				}
            	//float
				if ((ch == 'f') || (ch == 'F'))
				{
					buffer.append(ch);
					nextCh();
					return new TokenInfo(FLOAT_LITERAL, buffer.toString(), line);
				}
				//double
				else if ((ch == 'd') || (ch == 'D'))
					{
						buffer.append(ch);
						nextCh();
						return new TokenInfo(DOUBLE_LITERAL, buffer.toString(), line);
					}
			   else 
					{
						return new TokenInfo(DOUBLE_LITERAL, buffer.toString(), line);
					}
            }
            
            else if(ch == 'l' || ch == 'L')
            	//(1-9) {0-9} (l | L)
				{
					buffer.append(ch);
					nextCh();
					return new TokenInfo(LONG_LITERAL, buffer.toString(), line);
				}
		  else if ((ch == 'f') || (ch == 'F'))
				//float
					{
						buffer.append(ch);
						nextCh();
						return new TokenInfo(FLOAT_LITERAL, buffer.toString(), line);
					}
		else if ((ch == 'd') || (ch == 'D'))
			//double
						{
							buffer.append(ch);
							nextCh();
							return new TokenInfo(DOUBLE_LITERAL, buffer.toString(), line);
						}
		else // int
			return new TokenInfo(INT_LITERAL, buffer.toString(), line);  
            
            
        default:
            if (isIdentifierStart(ch)) {
                buffer = new StringBuffer();
                while (isIdentifierPart(ch)) {
                    buffer.append(ch);
                    nextCh();
                }
                String identifier = buffer.toString();
                if (reserved.containsKey(identifier)) {
                    return new TokenInfo(reserved.get(identifier),
                        line);
                } else {
                    return new TokenInfo(IDENTIFIER, identifier, line);
                }
            } else {
                reportScannerError("Unidentified input token: '%c'", ch);
                nextCh();
                return getNextToken();
            }
        }
    }

    /**
     * Scan and return an escaped character.
     * 
     * @return escaped character.
     */

    private String escape() {
        switch (ch) {
        case 'b':
            nextCh();
            return "\\b";
        case 't':
            nextCh();
            return "\\t";
        case 'n':
            nextCh();
            return "\\n";
        case 'f':
            nextCh();
            return "\\f";
        case 'r':
            nextCh();
            return "\\r";
        case '"':
            nextCh();
            return "\"";
        case '\'':
            nextCh();
            return "\\'";
        case '\\':
            nextCh();
            return "\\\\";
        case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
			StringBuffer escapeBuffer = new StringBuffer();

			if (ch > '3') {
				escapeBuffer.append(ch);
				nextCh();
				if (isOctal(ch)) {
					escapeBuffer.append(ch);
					nextCh();
					int i = Integer.parseInt(escapeBuffer.toString(), 8);
					if (i == 34) {
						return "\"";
					} else if (i == 39) {
						return "\'";
					} else {
						return "" + (char) i;
					}
				} else {
					int i = Integer.parseInt(escapeBuffer.toString(), 8);
					return "" + (char) i;
				}
			} else {
				escapeBuffer.append(ch);
				nextCh();
				if (isOctal(ch)) {
					escapeBuffer.append(ch);
					nextCh();
					if (isOctal(ch)) {
						escapeBuffer.append(ch);
						nextCh();
						int i = Integer.parseInt(escapeBuffer.toString(), 8);
						if (i > 127) {							
							reportScannerError("Unsupported Character.  %s " 
						                               ,escapeBuffer.toString());
						  return "";							
						} else if (i == 34) {
							return "\"";
						}else if (i == 39) {
							return "\'";
						} else {
							return "" + (char) i;
						}
					} else {
						int i = Integer.parseInt(escapeBuffer.toString(), 8);
						if (i == 34) {
							return "\"";
						} else if (i == 39) {
							return "\'";
						} else {
							return "" + (char) i;
						}
					}

				} else {
					int i = Integer.parseInt(escapeBuffer.toString(), 8);
					return "" + (char) i;
				}
			}

		default:
			reportScannerError("Badly formed escape: \\%c", ch);
			nextCh();
			return "";
		}
	}

    /**
	 * Scan and return an UnicodeEscape character.
	 * 
	 * @param x
	 *            s for string, c for character
	 * 
	 * @return Ascii value of UnicodeEscape character.
	 */
	private String unicode(char x) {
		StringBuffer tempBuffer = new StringBuffer();
		for (int i = 0; i < 4; i++) {
			if (!isHexDigit(ch)) {
				if (x == 's'){
					while(ch != '\"' && ch != ';' && ch != '\n' && ch != EOFCH){
						tempBuffer.append(ch);
						nextCh();
					}
				}
				if (x == 'c'){
					while(ch != '\'' && ch != ';' && ch != '\n' && ch != EOFCH){
						tempBuffer.append(ch);
						nextCh();
					}
				}
				reportScannerError("Badly formed unicode. %s", tempBuffer.toString());
				return "";
			} else {
				
				tempBuffer.append(ch);
				nextCh();
			}
		}

		String c = tempBuffer.toString();
		if (c.equals("005c") || c.equals("005C")) {
			return escape();
		} 
		else if (x == 's' && c.equals("0022") || c.equals("000a")
				|| c.equals("000A") || c.equals("000d") || c.equals("000D") 
				|| c.equals("000c") || c.equals("000C")) {			
			reportScannerError("illegal character: \\u",tempBuffer.toString());
			return "";
		} 
		else if (x == 'c' && c.equals("0027") || c.equals("000a")
				|| c.equals("000A") || c.equals("000d") || c.equals("000D")
				|| c.equals("000c") || c.equals("000C")) {
			reportScannerError("illegal character: \\u", tempBuffer.toString());
			return "";
		} 
		else {
			int i = Integer.parseInt(c, 16);
			if (i > 127) {
				reportScannerError("Unsupported Character: %s", c);
				return "";
			} else {
				return "" + (char) i;
			}
		}

	}
    
    
    /**
     * Advance ch to the next character from input, and update
     * the line number.
     */

    private void nextCh() {
        line = input.line();
        try {
            ch = input.nextChar();
        } catch (Exception e) {
            reportScannerError("Unable to read characters from input");
        }
    }

    /**
     * Report a lexcial error and record the fact that an error
     * has occured. This fact can be ascertained from the Scanner
     * by sending it an errorHasOccurred() message.
     * 
     * @param message
     *                message identifying the error.
     * @param args
     *                related values.
     */

    private void reportScannerError(String message, Object... args) {
        isInError = true;
        System.err.printf("%s:%d: ", fileName, line);
        System.err.printf(message, args);
        System.err.println();
    }

    /**
     * Return true if the specified character is a digit (0-9);
     * false otherwise.
     * 
     * @param c
     *                character.
     * @return true or false.
     */

    private boolean isDigit(char c) {
        return (c >= '0' && c <= '9');
    }
    
    /**
     * Return true if the specified character is a hex digit (0-9) | (a-f) | (A-F);
     * false otherwise.
     * 
     * @param c
     *                character.
     * @return true or false.
     */

    private boolean isHexDigit(char c) {
        return ((c >= '0' && c <= '9') || (c >= 'a' && c<= 'f') || (c >= 'A' && c <= 'F'));
    }
    
    /**
     * Return true if the specified character is a octal digit (0-7);
     * false otherwise.
     * 
     * @param c
     *                character.a
     * @return true or false.
     */

    private boolean isOctal(char c) {
        return (c >= '0' && c <= '7');
    }
    
    /**
     * checks 	 validity {0-9} [(e | E) [+ | -] (0-9) {0-9}]
     * if (e | E) seen calls scanDigit to make shure that it is
     * followed by digit
     * 
     * @param buffer
     *                StringBuffer
     * @return buffer.    
	 * 
	 */
	private StringBuffer scanFloat (StringBuffer buffer)
	{
		while (isDigit(ch)) 
		{
			buffer.append(ch);
			nextCh();
		}
		if ((ch == 'e') || (ch == 'E'))
		{
			buffer.append(ch);
			nextCh();
			buffer = scanDigit(buffer);
			return buffer;
		}
		else 
		{
			return buffer;

		}
	}
	 /**
     * checks 	 validity   [+ | -] (0-9) {0-9}
     * scan buffer to find digit, if found returns buffer
     * if not set isFloat = false and returns an error 
     * 
     * @param buffer
     *                StringBuffer
     * @return buffer.    
	 * 
	 */
	private StringBuffer scanDigit (StringBuffer buffer)
	{
		if ((ch == '+') || (ch == '-'))
		{
			buffer.append(ch);
			nextCh();
		}
		if(isDigit(ch))
		{
			while (isDigit(ch)) 
			{
				buffer.append(ch);
				nextCh();
			}
		}
		else if ((ch == 'f') || (ch == 'F'))
		{
			// after + or - is f or F
           buffer.append(ch);
    	   reportScannerError("Not valid  FLOAT literal. %s", buffer.toString());
           isFloat=false;
           nextCh();
		}
		else if ( (ch == 'd') ||(ch == 'D'))
		{
			//  after + or - d or D
           buffer.append(ch);
    	   reportScannerError("Not valid  Double literal. %s", buffer.toString());
           isFloat=false;
           nextCh();
		}
		
		else 
		{
			//no digit after + or -
           buffer.append(ch);
    	   reportScannerError("Not valid   literal. %s", buffer.toString());
           isFloat=false;			
		}
		return buffer;
	}


    /**
     * Return true if the specified character is a whitespace;
     * false otherwise.
     * 
     * @param c
     *                character.
     * @return true or false.
     */

    private boolean isWhitespace(char c) {
        switch (c) {
        case ' ':
        case '\t':
        case '\n': // CharReader maps all new lines to '\n'
        case '\f':
            return true;
        }
        return false;
    }
    
 

    /**
     * Return true if the specified character can start an
     * identifier name; false otherwise.
     * 
     * @param c
     *                character.
     * @return true or false.
     */

    private boolean isIdentifierStart(char c) {
        return (c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z'
            || c == '_' || c == '$');
    }
    
    

    /**
     * Return true if the specified character can be part of an
     * identifier name; false otherwise.
     * 
     * @param c
     *                character.
     * @return true or false.
     */

    private boolean isIdentifierPart(char c) {
        return (isIdentifierStart(c) || isDigit(c));
    }

    /**
     * Has an error occurred up to now in lexical analysis?
     * 
     * @return true or false.
     */

    public boolean errorHasOccurred() {
        return isInError;
    }

    /**
     * The name of the source file.
     * 
     * @return name of the source file.
     */

    public String fileName() {
        return fileName;
    }
}

/**
 * A buffered character reader. Abstracts out differences between
 * platforms, mapping all new lines to '\n'. Also, keeps track of
 * line numbers where the first line is numbered 1.
 */

class CharReader {

    /** A representation of the end of file as a character. */
    public final static char EOFCH = (char) -1;

    /** The underlying reader records line numbers. */
    private LineNumberReader lineNumberReader;

    /** Name of the file that is being read. */
    private String fileName;

    /**
     * Construct a CharReader from a file name.
     * 
     * @param fileName
     *                the name of the input file.
     * @exception FileNotFoundException
     *                    if the file is not found.
     */

    public CharReader(String fileName) throws FileNotFoundException {
        lineNumberReader = new LineNumberReader(new FileReader(
            fileName));
        this.fileName = fileName;
    }

    /**
     * Scan the next character.
     * 
     * @return the character scanned.
     * @exception IOException
     *                    if an I/O error occurs.
     */

    public char nextChar() throws IOException {
        return (char) lineNumberReader.read();
    }

    /**
     * The current line number in the source file, starting at 1.
     * 
     * @return the current line number.
     */

    public int line() {
        // LineNumberReader counts lines from 0.
        return lineNumberReader.getLineNumber() + 1;
    }

    /**
     * Return the file name.
     * 
     * @return the file name.
     */

    public String fileName() {
        return fileName;
    }

    /**
     * Close the file.
     * 
     * @exception IOException
     *                    if an I/O error occurs.
     */

    public void close() throws IOException {
        lineNumberReader.close();
    }
}
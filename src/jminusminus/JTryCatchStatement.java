/**
 * author : Jordan Hourani
 * added as part of homework #3 - Parser
 */

package jminusminus;

import java.util.ArrayList;

/**
 * The AST node for a catch-statement. 
 */

class JTryCatchStatement extends JStatement {

	/** the formal parameter */
	ArrayList<JFormalParameter> formalParams;
	
	/** a block statement */
	JBlock block;

    /**
     * Construct an AST node for a catch-statement given its line number, and
     * the formal parameter.
     * 
     * @param line
     *            line in which the catch-statement appears in the source file.
     * @param formalParam
     *            the formal parameter.
     */

    public JTryCatchStatement(int line, ArrayList<JFormalParameter> formalParam, 
    		JBlock block) {
        super(line);
        this.formalParams = formalParam;
        this.block = block;
    }

    /**
     * TODO
     */

    public JStatement analyze(Context context) {
        return null;
    }

    /**
     * 
     */

    public void codegen(CLEmitter output) {
        //TODO
    }

    /**
     * @inheritDoc
     */

    public void writeToStdOut(PrettyPrinter p) {
        if (formalParams != null) {
            p.printf("<JTryCatchStatement line=\"%d\">\n", line());
            p.indentRight();
            for (JFormalParameter param : formalParams)
            	param.writeToStdOut(p);
            p.indentLeft();
            p.printf("</JTryCatchStatement>\n");
        } else {
            p.printf("<JTryCatchStatement line=\"%d\"/>\n", line());
        }
    }

}

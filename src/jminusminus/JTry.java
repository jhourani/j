// Copyright 2011 Bill Campbell, Swami Iyer and Bahar Akbal-Delibas

package jminusminus;

import java.util.ArrayList;

/**
 * The AST node for a try block
 */

class JTry
    extends JStatement implements JMember{

    /** List of statements forming the try block body. */
    private JBlock block;
    
    /** List of statements forming the finally block body. */
    private JBlock finally_b;

    /** The list catches. */
    private ArrayList<JCatch> catches;
    
    /**
     * The new context (built in analyze()) represented by this
     * block.
     */
    private LocalContext context;

    /**
     * Construct an AST node for a try block given its line number,
     * and the list of statements forming the block body.
     * 
     * @param line
     *                line in which the try block occurs in the
     *                source file.
     * @param statements
     *                list of statements forming the block body.
     */

    /**
     * Construct an AST node for a try block given its line number,
     * and the list of statements forming the block body.
     * 
     * @param line
     *                line in which the try block occurs in the
     *                source file.
     * @param block
     * 				  list of statement forming a try block
     * @param catches	
     * 				  list of catches
     * @param finally_b
     * 				  list of statement forming a finally block
     */
    public JTry(int line, JBlock block, 
    		ArrayList<JCatch> catches, JBlock finally_b) {
        super(line);
        this.block = block;
        this.catches = catches;
        this.finally_b = finally_b;
    }

    /**
     * Return the list of statements comprising the try block.
     * 
     * @return list of statements.
     */

    public JBlock block() {
        return block;
    }

    /**
     * Analyzing a block consists of creating a new nested
     * context for that block and analyzing each of its
     * statements within that context.
     * 
     * @param context
     *                context in which names are resolved.
     * @return the analyzed (and possibly rewritten) AST subtree.
     */

    public JTry analyze(Context context) {
        return this;
    }

    /**
     * Generating code for a block consists of generating code
     * for each of its statements.
     * 
     * @param output
     *                the code emitter (basically an abstraction
     *                for producing the .class file).
     */

    public void codegen(CLEmitter output) {
    }
    /**
     * 
     * @param context
     * @param partial
     */
    public void preAnalyze(Context context, CLEmitter partial) {
    }

    /**
     * @inheritDoc
     */

    public void writeToStdOut(PrettyPrinter p) {
    	p.printf("<JTry line=\"%d\">\n", line());
    	p.indentRight();
        p.printf("<Tryblock line=\"%d\">\n", line());
        if (context != null) {
            p.indentRight();
            context.writeToStdOut(p);
            p.indentLeft();
        }
        if (block != null) {
            p.indentRight();
            block.writeToStdOut(p);
            p.indentLeft();
        }
        p.printf("</Tryblock>\n");
        if (catches != null) {
            p.println("<Catches>");
            for (JCatch catch_1 : catches) {
                p.indentRight();
                catch_1.writeToStdOut(p);
                p.indentLeft();
            }
            p.println("</Catches>");
        }
        if (finally_b != null) {
            p.println("<FinallyBlock>");
            p.indentRight();
            finally_b.writeToStdOut(p);
            p.indentLeft();
            p.println("</FinallyBlock>");
        }
        p.indentLeft();
        p.println("</JTry>");
    }
}
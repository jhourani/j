package jminusminus;

import static jminusminus.CLConstants.*;

import java.util.ArrayList;

/**
 * The AST node for a forInit statement with a FINAL
 */

class JFinalForInitStatement extends JExpression {

    private Type type;
    
    private ArrayList<JVariableDeclarator> varDecl;

    /**
     * Construct an AST node for a forInit part with a FINAL.
     * 
     * @param line
     *            line in which the for-statement occurs in the source file.
     * @param initPrt
     * 			  initialization part of a FOR loop 
     */

    public JFinalForInitStatement(int line, Type type, 
    		ArrayList<JVariableDeclarator> varDecl) {
        super(line);
        this.type = type;
        this.varDecl = varDecl;
    }

    /**
     * TODO
     */

    public JExpression analyze(Context context) {
        return null;
    }

    /**
     * TODO
     */

    public void codegen(CLEmitter output) {
        ;
    }

    /**
     * @inheritDoc
     */

    public void writeToStdOut(PrettyPrinter p) {
    	 p.printf("<JFinalForInitStatement line=\"%d\">\n", line());
         p.printf("</JFinalForInitStatement>\n");
    }

}

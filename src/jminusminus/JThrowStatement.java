// Copyright 2013 Bill Campbell, Swami Iyer and Bahar Akbal-Delibas

package jminusminus;

import static jminusminus.CLConstants.*;

/**
 * The AST node for a throw-statement. 
 */

class JThrowStatement extends JStatement {

    /** The returned expression. */
    private JExpression expr;

    /**
     * Construct an AST node for a throw-statement given its line number, and
     * the expression that is returned.
     * 
     * @param line
     *            line in which the throw-statement appears in the source file.
     * @param expr
     *            the returned expression.
     */

    public JThrowStatement(int line, JExpression expr) {
        super(line);
        this.expr = expr;
    }

    /**
     * TODO
     */

    public JStatement analyze(Context context) {
        return null;
    }

    /**
     * TODO
     */

    public void codegen(CLEmitter output) {
        ;
    }

    /**
     * @inheritDoc
     */

    public void writeToStdOut(PrettyPrinter p) {
        if (expr != null) {
            p.printf("<JThrowStatement line=\"%d\">\n", line());
            p.indentRight();
            expr.writeToStdOut(p);
            p.indentLeft();
            p.printf("</JThrowStatement>\n");
        } else {
            p.printf("<JThrowStatement line=\"%d\"/>\n", line());
        }
    }

}

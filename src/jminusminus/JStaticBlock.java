// Copyright 2013 Bill Campbell, Swami Iyer and Bahar Akbal-Delibas

package jminusminus;

import java.util.ArrayList;

/**
 * The AST node for a block, which delimits a nested level of scope.
 */

class JStaticBlock extends JBlock implements JMember {

    /** List of statements forming the block body. */
    private ArrayList<JStatement> statements;

    /**
     * The new context (built in analyze()) represented by this block.
     */
    private LocalContext context;

    /**
     * Construct an AST node for a block given its line number, and the list of
     * statements forming the block body.
     * 
     * @param line
     *            line in which the block occurs in the source file.
     * @param statements
     *            list of statements forming the block body.
     */

    public JStaticBlock(int line, ArrayList<JStatement> statements) {
        super(line, statements);
    }

    /**
     * Return the list of statements comprising the block.
     * 
     * @return list of statements.
     */

    public ArrayList<JStatement> statements() {
        return statements;
    }

    /**
     * TODO
     */

    public JBlock analyze(Context context) {
        
        return null;
    }

    /**
     * TODO
     */

    public void codegen(CLEmitter output) {
    }

    /**
     * @inheritDoc
     */

    public void writeToStdOut(PrettyPrinter p) {
        p.printf("<JStaticBlock line=\"%d\">\n", line());
        /*if (context != null) {
            p.indentRight();
            context.writeToStdOut(p);
            p.indentLeft();
        }*/
        for (JStatement statement : statements) {
            p.indentRight();
            statement.writeToStdOut(p);
            p.indentLeft();
        }
        p.printf("</JStaticBlock>\n");
    }

	@Override
	public void preAnalyze(Context context, CLEmitter partial) {
		// TODO Auto-generated method stub
		
	}

}

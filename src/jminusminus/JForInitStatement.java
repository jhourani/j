
package jminusminus;

import static jminusminus.CLConstants.*;

import java.util.ArrayList;

/**
 * The AST node for a forInit statement
 */

class JForInitStatement extends JExpression {

    
    private ArrayList<JStatement> initPart;

    /**
     * Construct an AST node for an if-statement given its line number, the test
     * expression, the consequent, and the alternate.
     * 
     * @param line
     *            line in which the if-statement occurs in the source file.
     * @param initPrt
     * 			  initialization part of a FOR loop 
     */

    public JForInitStatement(int line, ArrayList<JStatement> initPart) {
        super(line);
        this.initPart = initPart;
    }

    /**
     * TODO
     */

    public JExpression analyze(Context context) {
        return null;
    }

    /**
     * TODO
     */

    public void codegen(CLEmitter output) {
        ;
    }

    /**
     * @inheritDoc
     */

    public void writeToStdOut(PrettyPrinter p) {
    	 p.printf("<JForInitStatement line=\"%d\">\n", line());
         p.printf("</JForInitStatement>\n");
    }

}

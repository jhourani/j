// Copyright 2011 Bill Campbell, Swami Iyer and Bahar Akbal-Delibas

package jminusminus;
import java.util.ArrayList;
import static jminusminus.CLConstants.*;

/**
 * The AST node for a for-statement.
 */

class JForStatement
    extends JStatement {

	/** Initialization expression. */
    private ArrayList<JStatement> init;

    /** Test expression. */
    private JStatement condition;
    
    /** Update expression. */
    private ArrayList<JStatement> update;

    /** The body. */
    private JStatement body;
    
    /**
     * Construct an AST node for a for-statement given its line
     * number, the test expression, and the body.
     * 
     * @param line
     *                line in which the while-statement occurs in
     *                the source file.
     * @param init
     * 				  list of initializers
     * @param condition
     *                test expression.
     * @param update
     * 				  list of Update statements
     * @param body
     *                the body.
     */
    public JForStatement(int line, ArrayList<JStatement> init, 
        JStatement condition, ArrayList<JStatement> update,
        JStatement body) {
        super(line);
        this.init = init;
        this.update = update;
        this.condition = condition;
        this.body = body;
    }

    /**
     * Analysis involves analyzing the test, checking its type
     * and analyzing the body statement.
     * 
     * @param context
     *                context in which names are resolved.
     * @return the analyzed (and possibly rewritten) AST subtree.
     */

    public JForStatement analyze(Context context) {
        return this;
    }

    /**
     * Generate code for the for loop. 
     * 
     * @param output
     *                the code emitter (basically an abstraction
     *                for producing the .class file).
     */

    public void codegen(CLEmitter output) {
    }

    /**
     * @inheritDoc
     */

    public void writeToStdOut(PrettyPrinter p) {
        p.printf("<JForStatement line=\"%d\">\n", line());
        p.indentRight();
        if (init != null) {
            p.println("<Initializer>");
            for (JStatement initialized : init) {
                p.indentRight();
                initialized.writeToStdOut(p);
                p.indentLeft();
            }
            p.println("</Initializer>");
        }
        if (condition != null) {
        	p.printf("<TestExpression>\n");
            p.indentRight();
            condition.writeToStdOut(p);
            p.indentLeft();
            p.printf("</TestExpression>\n");
        }
        if (update != null) {
            p.println("<Updates>");
            for (JStatement updated : update) {
                p.indentRight();
                updated.writeToStdOut(p);
                p.indentLeft();
            }
            p.println("</Updates>");
        }
        
        p.printf("<Body>\n");
        p.indentRight();
        body.writeToStdOut(p);
        p.indentLeft();
        p.printf("</Body>\n");
        p.indentLeft();
        p.printf("</JForStatement>\n");
    }
}
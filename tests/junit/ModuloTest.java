package junit;

import pass.Modulo;
import junit.framework.TestCase;

/**
 * Class to test the modulo (%) operator. Only checks the
 * positive case for now. 
 * @author Jordan
 *
 */
public class ModuloTest extends TestCase {
	private Modulo mod;
	
	protected void setUp() throws Exception {
		super.setUp();
		mod = new Modulo();
	}
	
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	@SuppressWarnings("static-access")
	public void testDivide() {
		this.assertEquals(mod.modulus(10, 2), 0);
		this.assertEquals(mod.modulus(20, 3), 2);
	}
}
package junit;

import pass.Division;
import pass.Operators;
import junit.framework.TestCase;


public class OperatorsTest extends TestCase {

	public Operators test;
	
	protected void setUp() throws Exception {
		super.setUp();
		test = new Operators();
	}
	
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	@SuppressWarnings("static-access")
	public void testOperators () {
		this.assertEquals(test.preInc(1), 2);
		this.assertEquals(test.postInc(1), 2);
		this.assertEquals(test.postDec(1), 1);
		
		// these added for homework #2
		this.assertEquals(test.bitwiseAnd(1), 1);
		this.assertTrue(test.greaterOrEqual(1, 1));
		this.assertEquals(test.unsignedRShift(-1), 25);
		this.assertEquals(test.signedRShift(10), 1);
		this.assertEquals(test.signedLShift(1), 8);
		this.assertFalse(test.lessThan(1, 10));
		this.assertEquals(test.exclusiveOr(0, 1), 1);
		this.assertEquals(test.inclusiveOr(0, 1), 1);
		this.assertTrue(test.logicalOr(true));
		this.assertEquals(test.compliment(0x1010), 0x0101);
		this.assertEquals(test.divEq(10, 2), 5);
		this.assertEquals(test.modEq(10, 2), 0);
	}
}
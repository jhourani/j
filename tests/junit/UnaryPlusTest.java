package junit;

import pass.UnaryPlus;
import junit.framework.TestCase;

public class UnaryPlusTest extends TestCase {
	private UnaryPlus uplus;
	
	protected void setUp() throws Exception {
		super.setUp();
		uplus = new UnaryPlus();
	}
	
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	@SuppressWarnings("static-access")
	public void testDivide() {
		this.assertEquals(uplus.unaryPlus(+4), 4);
		this.assertEquals(uplus.unaryPlus(+-4), -4);
	}
}

package junit;

import pass.Division;
import junit.framework.TestCase;

/**
 * Test case added to check that the results of performing
 * the division operation are the desired ones.
 * @author Jordan
 *
 */
public class DivisionTest extends TestCase {
	private Division division;
	
	protected void setUp() throws Exception {
		super.setUp();
		division = new Division();
	}
	
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	// Decided to ignore the static warning for now.
	@SuppressWarnings("static-access")
	public void testDivide() {
		this.assertEquals(division.divide(0, 42), 0);
		this.assertEquals(division.divide(42, 1), 42);
		this.assertEquals(division.divide(127, 3), 42);
	}
}

package pass;

public class Operators {
	
	public int preInc(int x) {
		return ++x;
	}

	public int postInc(int x) {
		return x++;
	}

	public int preDec(int x) {
		return --x;
	}
	
	public int postDec(int x) {
		return x--;
	}
	
	public int bitwiseAnd(int x) {
		return x & 1;
	}
	
	//public boolean greaterOrEqual(int x, int y) {
	//	if (x >= y)
	//		return true;
	//	else
	//		return false;
	//}
	
	public int unsignedRShift(int x) {
		return x >>> 24;
	}
	
	public int signedRShift(int x) {
		return x >> 3;
	}
	
	public int signedLShift(int x) {
		return x << 3;
	}
	
	public boolean lessThan(int x, int y) {
		if (x < y)
			return true;
		else 
			return false;
	}
	
	public int exclusiveOr(int x, int y) {
		return x ^ y;
	}
	
	public int inclusiveOr(int x, int y) {
		return x | y;
	}
	
	public boolean logicalOr(boolean x) {
		return x || true;
	}

	public int modEq(int x, int y) {
		return x %= y;
	}
	
	public int divEq(int x, int y) {
		return x /= y;
	}
	
	//public int compliment(int x) {
	//	return ~x;
	//}
}

package pass;

public class Literals {

	public double double_test = 5.42;
	public long long_test = 22342L;
	public float float_test = 5.42F;
	public int octal_test = 0234;
	public int hex_test1 = 0x1234;
	public int hex_test2 = 0x123A;
	
	// Test the keywords
//	assert break byte case catch const continue default do double enum float
//	final finally for goto implements interface long native strictfp 
//	synchronized throw throws transient try volatile
	
	public double testDouble(double x, double y) {
		return x + y;
	}
	
	public float testFloat(float x, float y) {
		return x + y;
	}
	
	public long testLong(long x, long y) {
		return x + y;
	}
	
	public int testHex(int x, int y) {
		return x + y;
	}
	
	public int testOctal(int x, int y) {
		return x + y;
	}
}

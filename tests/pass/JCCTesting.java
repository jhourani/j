package pass;

/**
 * This is just a test class set up for testing the changes 
 * to the JavaCC Scanner and Parser for homework 4.
 * 
 * @author Jordan
 *
 */
public class JCCTesting {
	
	// testing int literals
	int i = 2;
	int i2 = i;
	
	// testing oct-representation
	int oct = 0341;
	
	// testing hex representations of ints
	int j = 0x234;
	int k = 0XABCD;
	int m = 0x3Fa9;
	
	// testing the long literal. Upper and lowercase.
	long a = 222l;
	long b = 111L;
	
	// testing a static block
	static {
		char c  ='d';
		String s = c + "d";
	}
	
	// testing the do-while statement
	public void testDoWhile() {
		int z = 1;
		int y = 4;
		
		do {
			z = z + 1;
			y = y - 1;
		}
		while ( y > 0);
	}
	
	// testing an instance block.
	//{
	//	char e  ='e';
	//	String s1 = e + "e";
	//}
}

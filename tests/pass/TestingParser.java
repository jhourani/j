package pass;

import java.util.ArrayList;

/**
 * Test class for the Parser. This class will test all new implementations
 * for homework 3.
 * 
 * @author Jordan
 *
 */


// Test implements by implementing Comparable.
public class TestingParser implements Interface {
	
	/* static block should be defined at start of a class, so it is being
	 * tested here to generate a pass.
	 */
	static {
		int x = 1;
	}

	int x = 2;
	int y = 3;
	
	// The literals
	double d = 2.34;
	double d1 = 0.234;
	double d3 = 3.2000;
	double d4 = 4.32e2;
	
	long l = 123L;
	float f = 2.56f;
	float f1 = 2.56e2f;
	
	// Other representations of integers.
	int hex = 0x4fff;
	int oct = 0341;
	

	ArrayList ints = new ArrayList();
	
	// Test all the implemented java operators.
	public void testOperators(int a) {
		int b = a;
		int c = a | b;
		int d = a ^ b;
		int e = a & b;
		b -= 1;
		b *= 20;
		b /= 2;
		b %= 2;
		b >>= 1;
		b >>>= 1;
		b <<= 1;
		b &= 2;
		b ^= 2;
		b |= 1;
	}
	
	// Conditional expressions and Ternary (?) expression.
	public void testConditionalExpressions() {
		boolean t = true;
		boolean f = false;
		boolean z = true;
		boolean y = false;
		
		int x = f == true ? 1 : 3;
		boolean tf = t | f;
		 
		
		// inclusive OR
		int a = 1, b = 0;
		if (a == 1 || b == 1)
			;
		
		// inclusive AND
		//int d = 1, e = 0;
		//if (a == 1 && b == 1)
	//		;
	}
	
	/**
	 * Testing the 'throws' clause.
	 * 
	 * @throws Exception
	 */
	public void addIt() throws Exception {
		int x = 0;
	}
	
	// Test the 'throw' statement.
	public void throwStatement() throws Exception {
		int z;
		int x = 5;
		int y = 0;
		if (y == 0)
			throw new Exception();
		else
			 z = x/y;	
	}
	
	/**
	 *  Test the try-catch-finally statement.
	 */
	public void tryCatch() {
		int x = 2;
		try {
			x = x++;
		}
		catch (Exception e) {
			new Exception();
		}
		finally {
			x = 0;
		}
	}
	
	/**
	 *  Method to test a regular FOR loop.
	 */
	public void regularFor() {
		int x = 0;
		for (int y = 1; y < 10; y++)
			x++;
	}
	
	/**
	 * Method to test the enhanced-FOR loop.
	 */
	public void enhancedFor() {
		int x = 0;
		ints.add(1);
		ints.add(2);
		ints.add(3);
		for (Object i : ints)
			x++;
	}
	
	/**
	 * Test the do-while statement.
	 */
	public void doWhile() {
		int x = 0;
		do {
			x++;
		}
		while (x < 10);
	}
	
	/**
	 * Instance block test.
	 */
	public void instanceBlockTest() {
		//{
		//	String s = "just testing";
		//}
	}
	
	/**
	 * Using variable-arity
	 * 
	 * @param args
	 */
	public void variableArity(Integer... args) {
		//do nothing
	}
}

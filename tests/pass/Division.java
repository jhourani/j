package pass;

// a one line comment 

/*
 * A multi-line testing comment.
 */

public class Division {
	public int divide(int x, int y) {
		return x / y;
	}
}
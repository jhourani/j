package junit;

import pass.Modulo;
import junit.framework.TestCase;

public class ModuloTest extends TestCase {
		
		private Modulo modulo;
		
		protected void setUp() throws Exception {
			super.setUp();
			modulo = new Modulo();
		}
		
		protected void tearDown() throws Exception {
			super.tearDown ();
		}
		
		public void testDivide() {
			this.assertEquals(modulo.modulus(5, 5), 0);
			this.assertEquals(modulo.modulus(20, 3), 2);
		}
}
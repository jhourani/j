package old_pass;

import junit.framework.TestCase;


public class LiteralsTest extends TestCase {

	// Test the added literals
	public double double_test = 5.42;
	public long long_test = 22342L;
	public float float_test = 5.42F;
	public int hex_test1 = 0x1234;
	public int hex_test2 = 0x123A;
	//public int octal_test = 0234;
	
	// Test the keywords
	assert break byte case catch const continue default do double enum float
	final finally for goto implements interface long native strictfp 
	synchronized throw throws transient try volatile
	
	
//	protected void setUp() throws Exception {
//		super.setUp();
//		test = new Literals();
//	}
	
//	protected void tearDown() throws Exception {
//		super.tearDown();
//	}
	
//	@SuppressWarnings("static-access")
//	public void testLiterals() {
//		this.assertEquals(test.testDouble(1.0, 1.5), 2.5);
//		this.assertEquals(test.testFloat(1.0f, 1.5f), 2.5f);
//		this.assertEquals(test.testLong(2L, 2L), 4L);
//		this.assertEquals(test.testHex(0x10, 0x10), 0x20);
//		this.assertEquals(test.testOctal(0234, 0234), 468);
//	}
}